---
title: "encevi"
author: "Amanda Ullman"
date: "3/2/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r}
rm(list=ls())

#Load Packages
if (!require("pacman")) install.packages("pacman")
  pacman::p_load(pacman, party, psych, rio, BiocManager, tidyverse, ggplot2, magrittr, plyr, knitr, dplyr, ggplot2, survey, summarytools, ENmisc, dplyr, xtable, formattable, reshape, reshape2, tables)

opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)

aire<-read_csv("inputs/ENCEVI/aireacond.csv")
viv <-read_csv("inputs/ENCEVI/vivienda.csv")
encevi <- read_csv("inputs/ENCEVI/encevi.csv")

aire <- aire %>%
  select("folio", "aire_tipo", "aire_dia", "aire_hor", "aire_capa",)

viv<-viv %>%
  select("folio", "region", "est_socio", "est_dis", "entidad", "upm", "factor_sem")

encevi <- encevi %>%
   select("folio", "sello_air")

aire$aire_hor <- as.numeric(aire$aire_hor)
aire$aire_hor[aire$aire_hor==99]<-0   
aire$aire_hor[aire$aire_hor==00]<-0
aire$hour <- as.numeric(aire$aire_hor)
aire$hours <- cut(aire$aire_hor,
                 breaks = c(0,2,5,9,Inf),
                 labels = c("less than 2", "2 to 5", "5 to 9", "9+"))

viv$region <- as.factor(viv$region)
viv$climate<-revalue(viv$region, c('1' = 'Arid', '2' = 'Temperate', '3' = 'Tropical'))

aire$aire_capa <- as.factor(aire$aire_capa)
aire$capacity<-revalue(aire$aire_capa, c('1' = '<.75', '2' = '.75', '3' = '1', '4' = '1.5', '5' = '2','6'='>2', '9' = 'unknown'))

aire$aire_tipo <- as.factor(aire$aire_tipo)
aire$tipo<-revalue(aire$aire_tipo, c('1' = 'Portable', '2' = 'Window', '3' = 'Central', '4' = 'Evaporative', '5' = 'Minisplit_on/off','6'='Minisplit_inverter', '7' = 'Other', '9' = 'Unknown'))

 
df<- left_join(aire, viv,by="folio")
df<-left_join(df, encevi, by = "folio")
df$sello_air[df$sello_air==2]<-0
df$sello_air[df$sello_air==9]<-0

svd.fnew <- svydesign(id =~upm, data = df, weight=~factor_sem, na.rm = TRUE)

```

```{r}



#AC 
#proportion of households with energy saving bulbs
mean.total<- svyby(~capacity, by=~climate+tipo, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~capacity+climate+tipo, design = svd.fnew) %>%as.data.frame()

socio_heat<- merge(freq, mean.total2, by=c("climate", "tipo"))


write.csv(socio_heat,"~/Desktop/MP/mx_electricity_residential/outputs/encevi/climate.capacity.type2.csv", row.names = FALSE)

```

```{r}
mean.total<- svyby(~sello_air, by=~climate, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~sello_air+climate, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$sello_air == 1, ]
freq3<-freq[freq$sello_air == 0, ] %>%rename(c('Freq'='Freq2'))
freq2<-merge(freq2, freq3, by="climate")
freq2$total.households=freq2$Freq2+freq2$Freq

socio_ac<- merge(freq2, mean.total2, by=c("climate"))%>%select("climate", "sello_air", "se", "Freq", "total.households")%>%rename(c('sello_air'='Rate'))


write.csv(socio_ac,"~/Desktop/MP/mx_electricity_residential/outputs/ac/ac.sello.climadopt.csv", row.names = FALSE)
```

```{r}

#AC 
#proportion of households with energy saving bulbs
mean.total<- svyby(~climate, by=~sello_air, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~sello_air+climate, design = svd.fnew) %>%as.data.frame()

socio_heat<- merge(freq, mean.total2, by=c("climate", "sello_air"))


write.csv(socio_heat,"~/Desktop/MP/mx_electricity_residential/outputs/encevi/climate.sello.csv", row.names = FALSE)
```


```{r}
svymean(~aire_tipo*aire_capa, svd.fnew, na.rm = TRUE)
```