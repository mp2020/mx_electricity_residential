---
title: "R Notebook"
output: html_notebook
---
```{r setup, include =FALSE}
rm(list=ls())
if (!require("pacman")) install.packages("pacman")
  pacman::p_load(pacman, party, psych, rio, tidyverse, ggplot2, magrittr, plyr, knitr, dplyr, ggplot2, survey, summarytools, ENmisc)


opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
```

```{r Income Mexico, include =FALSE}
datapath<-  "https://mauricioh2.com/mp2020/inegi/enigh/"
households.2018 <-read_csv(paste0(datapath,"2018_enigh_hogares.csv"))

concentrated_households.2018<-read_csv(paste0(datapath, "2018_enigh_concentradohogar.csv"))

#concentrated_households.2018<-data.frame("folioviv"=concentrated_households.2018$folioviv, "energy"=concentrated_households.2018$energia,"income"=concentrated_households.2018$ing_cor, "ubica_geo"= concentrated_households.2018$ubica_geo, "est_socio"= concentrated_households.2018$est_socio, "upm"=concentrated_households.2018$upm, "est_dis"= concentrated_households.2018$est_dis,"factor"= concentrated_households.2018$factor)

energypoverty<-data.frame("folioviv"=concentrated_households.2018$folioviv, "energy"=concentrated_households.2018$energia,"income"=concentrated_households.2018$ing_cor, "ubica_geo"= concentrated_households.2018$ubica_geo, "est_socio"= concentrated_households.2018$est_socio, "upm"=concentrated_households.2018$upm, "est_dis"= concentrated_households.2018$est_dis,"factor"= concentrated_households.2018$factor)

ingresos.2018<-read_csv(paste0(datapath,"2018_enigh_ingresos.csv"))

#ingresos.2018<-data.frame("folioviv"=ingresos.2018$folioviv, "income_tri"=ingresos.2018$ing_tri)



gasto_hogar.2018<-read_csv(paste0(datapath,"2018_enigh_gastoshogar.csv"))

gasto_hogar.2018<-data.frame("folioviv"= gasto_hogar.2018$folioviv, "tot_expenses" = gasto_hogar.2018$gasto_tri)



#energypoverty<- merge(ingresos.2018, concentrated_households.2018, by="folioviv")

```

```{r rename_bystate, include = FALSE}
energypoverty<- as_tibble(energypoverty) 
#Mutate(ubica_geo = as.character(ubica_geo))


energypoverty$state.code <- substr(energypoverty$ubica_geo, 1, 2)

energypoverty$state <- factor(energypoverty$state.code, 
                     levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                     labels = c("Aguascalientes", "Baja California", 
                                "Baja California Sur", "Campeche", 
                                "Coahuila", "Colima", "Chiapas", 
                                "Chihuahua", "Mexico City", 
                                "Durango", "Guanajuato", "Guerrero", 
                                "Hidalgo", "Jalisco", "Mexico", 
                                "Michoacan", "Morelos", "Nayarit", 
                                "Nuevo Leon", "Oaxaca", "Puebla", 
                                "Queretaro", "Quintana Roo", 
                                "San Luis Potosi", "Sinaloa", 
                                "Sonora", "Tabasco", "Tamaulipas", 
                                "Tlaxcala", "Veracruz", "Yucatan", 
                                "Zacatecas"))

energypoverty$climate <- factor(energypoverty$state.code,
                   levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                   labels = c("Templada", "Calida.Ext", "Calida.Ext", "Tropical",
                              "Calida.Ext", "Templada", "Tropical", "Calida.Ext",
                              "Templada","Calida.Ext", "Templada", "Tropical",
                              "Templada", "Templada", "Templada", "Templada",
                              "Templada", "Templada", "Calida.Ext", "Tropical", 
                              "Templada", "Templada", "Tropical", "Templada",
                              "Calida.Ext", "Calida.Ext", "Tropical", "Calida.Ext",
                              "Templada", "Templada", "Tropical", "Templada"))

energypoverty$urb.code <- substr(energypoverty$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign category of rural or urban as label
energypoverty$urban <- factor(energypoverty$urb.code, 
                     levels = c("0", "1", "2", "3", "4", "5", "6", "7", 
                                "8", "9"),
                     labels = c("Urban", "Urban", "Urban", "Urban", "Urban", "Urban", 
                                "Rural","Urban", "Urban", "Urban"))
```

```{r energy_poverty_analysis, Include =TRUE}
apply(na.rm=)
energypoverty$energy_expense<- energypoverty$energy/energypoverty$income

summary_energypoverty<-summarytools::descr(select(energypoverty,c('income','energy', 'energy_expense')), weights = energypoverty$factor, style = "rmarkdown")
summary_energypoverty

```
