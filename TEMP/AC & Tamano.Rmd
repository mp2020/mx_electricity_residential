---
title: "AC & Tamano"
author: "Amanda Ullman"
date: "1/9/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r Clean Memory/ Auto Load, echo = F, include = F}
#Clean Memory
rm(list=ls())

#garbage collector
gc()

#Make packages auto load
if(!require("pacman")) install.packages("pacman")
```

```{r Packages, echo = F, include =F}
#load contribute packages with pacman
pacman::p_load(pacman, rio, tidyverse, bit64,forcats,gridExtra,lubridate,cowplot,summarytools,RColorBrewer,survey, scales, ENmisc, plyr, withr, weights, plotrix)
```

Load merged 2018 data
```{r load_data2018, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2018/"
Viv2018 <-read_csv(paste0(path.data, "2018_enigh_viviendas.csv"))
Gastos2018 <-read_csv(paste0(path.data, "2018_enigh_concentradohogar.csv"))
```

Load merged 2016 data
```{r load_data2016, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2016/"
Viv2016 <-read_csv(paste0(path.data, "2016_enigh_viviendas.csv"))
Gastos2016 <-read_csv(paste0(path.data, "2016_enigh_concentradohogar.csv"))
```

Load merged 2014 data
```{r load_data2014, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2014/"
Viv2014 <-read_csv(paste0(path.data, "2014_enigh_viviendas.csv"))
Gastos2014 <-read_csv(paste0(path.data, "2014_ENIGH_concentradohogar.csv"))
```

Load merged 2012 data
```{r load_data2012, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2012/"
Viv2012 <-read_csv(paste0(path.data, "2012_enigh_viviendas.csv"))
Gastos2012 <-read_csv(paste0(path.data, "2012_enigh_concentradohogares.csv"))
```

Load 2010 data
```{r load_data2010, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2010/"
Viv2010 <-read_csv(paste0(path.data, "2010_enigh_hogares.csv"))
Gastos2010 <-read_csv(paste0(path.data, "2010_enigh_concentrado.csv"))
```

```{r load_data2008, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2008/"
Viv2008 <-read_csv(paste0(path.data, "2008_enigh_hogares.csv"))
Gastos2008 <-read_csv(paste0(path.data, "2008_enigh_concentradohogares.csv"))
```

```{r load_databind, include = FALSE}
#Unifying 2008 with other tables
E2008<- data.frame("folioviv" = Viv2008$folioviv, "foliohog" = Viv2008$foliohog, "tipo_viv" = Viv2008$claviv, "antiguedad" = Viv2008$antigua, "ac" = Viv2008$aire_aco, "calefacc" = Viv2008$calef, "ubica_geo" = Viv2008$ubica_geo, "upm" = Viv2008$upm, "factor" = Viv2008$factor, "tam_loc" = Viv2008$estrato)
E2008$year<-1:29468
E2008$year<- rep("2008",length(E2008$year))
E2008$folioviv <- as.character(E2008$folioviv)
E2008$ubica_geo <- as.character(E2008$ubica_geo)
E2008$ubica_geo <- str_pad(E2008$ubica_geo, 5, pad = "0" )

# Rename a column in R

G2008<- data.frame("folioviv" = Gastos2008$folioviv,  "foliohog" = Gastos2008$foliohog, "ubica_geo" = Gastos2008$ubica_geo, "tenencia" = Gastos2008$tenencia, "num_cuarto" = Gastos2008$tam_hog,"ing_cor" = Gastos2008$ingcor, "energia" = Gastos2008$energia)
G2008$year<-1:29468
G2008$year<- rep("2008",length(G2008$year))
G2008$folioviv <- as.character(Gastos2008$folioviv)
G2008$ubica_geo <- as.character(Gastos2008$ubica_geo)
G2008$ubica_geo <- str_pad(G2008$ubica_geo, 5, pad = "0" )

#Joined Gastos & Viviendas
A2008 <- left_join(E2008, G2008)

#Unifying 2010 with other tables

E2010<- data.frame("folioviv" = Viv2010$folioviv, "foliohog" = Viv2010$foliohog, "tipo_viv" = Viv2010$claviv, "antiguedad" = Viv2010$antigua, "ac" = Viv2010$aire_aco, "calefacc" = Viv2010$calef, "ubica_geo" = Viv2010$ubica_geo, "upm" = Viv2010$upm, "factor" = Viv2010$factor, "tam_loc"=Viv2010$tam_loc)
E2010$year<-1:27655
E2010$year<- rep("2010",length(E2010$year))
E2010$folioviv <- as.character(E2010$folioviv)
E2010$ubica_geo <- as.character(E2010$ubica_geo)
E2010$upm <- as.character(E2010$upm)
E2010$ubica_geo <- str_pad(E2010$ubica_geo, 5, pad = "0" )

# Rename a column in R

G2010<- data.frame("folioviv" = Gastos2010$folioviv,  "foliohog" = Gastos2010$foliohog, "ubica_geo" = Gastos2010$ubica_geo, "tenencia" = Gastos2010$tenencia, "num_cuarto" = Gastos2010$tam_hog,"ing_cor" = Gastos2010$ingcor, "energia" = Gastos2010$energia, "tam_loc" = Viv2010$tam_loc)
G2010$year<-1:27655
G2010$year<- rep("2010",length(G2010$year))
G2010$folioviv <- as.character(Gastos2010$folioviv)
G2010$ubica_geo <- as.character(Gastos2010$ubica_geo)
E2010$ubica_geo <- str_pad(E2010$ubica_geo, 5, pad = "0" )

#Joined Gastos & Viviendas
A2010 <- left_join(E2010, G2010)

#Unifying 2012 with other tables

E2012<- data.frame("folioviv" = Viv2012$folioviv, "tipo_viv" = Viv2012$tipo_viv, "antiguedad" = Viv2012$antiguedad, "ac" = Viv2012$aire_acond, "calefacc" = Viv2012$calefacc, "ubica_geo" = Viv2012$ubica_geo, "upm" = Viv2012$upm, "tenencia" = Viv2012$tenencia, "num_cuarto" = Viv2012$num_cuarto, "factor" = Viv2012$factor_viv, "tam_loc"= Viv2012$tam_loc)
E2012$year<-1:8861
E2012$year<- rep("2012",length(E2012$year))
E2012$folioviv <- as.character(E2012$folioviv)
E2012$ubica_geo <- as.character(E2012$ubica_geo)
E2012$upm <- as.character(E2012$upm)

# Rename a column in R
G2012<- data.frame("folioviv" = Gastos2012$folioviv,  "foliohog" = Gastos2012$foliohog, "ubica_geo" = Gastos2012$ubica_geo,"ing_cor" = Gastos2012$ing_cor, "energia" = Gastos2012$energia)
G2012$year<-1:9002
G2012$year<- rep("2012",length(G2012$year))
G2012$folioviv <- as.character(Gastos2012$folioviv)
G2012$ubica_geo <- as.character(Gastos2012$ubica_geo)

#Joined Gastos & Viviendas
A2012 <- left_join(E2012, G2012)

#Unifying 2014 with other tables
E2014<- data.frame("folioviv" = Viv2014$folioviv, "tipo_viv" = Viv2014$tipo_viv, "antiguedad" = Viv2014$antiguedad, "ac" = Viv2014$aire_acond, "calefacc" = Viv2014$calefacc, "ubica_geo" = Viv2014$ubica_geo, "upm" = Viv2014$upm, "tenencia" = Viv2014$tenencia, "num_cuarto" = Viv2014$num_cuarto, "factor" = Viv2014$factor_viv, "tam_loc" = Viv2014$tam_loc)
E2014$year<-1:19124
E2014$year<- rep("2014",length(E2014$year))
E2014$folioviv <- as.character(E2014$folioviv)
E2014$ubica_geo <- as.character(E2014$ubica_geo)
E2014$upm <- as.character(E2014$upm)

# Rename a column in R
G2014<- data.frame("folioviv" = Gastos2014$folioviv,  "foliohog" = Gastos2014$foliohog, "ubica_geo" = Gastos2014$ubica_geo,"ing_cor" = Gastos2014$ing_cor, "energia" = Gastos2014$energia)
G2014$year<-1:19479
G2014$year<- rep("2014",length(G2014$year))
G2014$folioviv <- as.character(Gastos2014$folioviv)
G2014$ubica_geo <- as.character(Gastos2014$ubica_geo)

#Joined Gastos & Viviendas
A2014 <- left_join(E2014, G2014)

#Unifying 2016 with other tables
E2016<- data.frame("folioviv" = Viv2016$folioviv, "tipo_viv" = Viv2016$tipo_viv, "antiguedad" = Viv2016$antiguedad, "ac" = Viv2016$aire_acond, "calefacc" = Viv2016$calefacc, "ubica_geo" = Viv2016$ubica_geo, "upm" = Viv2016$upm, "tenencia" = Viv2016$tenencia, "num_cuarto" = Viv2016$num_cuarto, "factor" = Viv2016$factor, "tam_loc" = Viv2016$tam_loc)
E2016$year<-1:69169
E2016$year<- rep("2016",length(E2016$year))
E2016$folioviv <- as.character(E2016$folioviv)
E2016$ubica_geo <- as.character(E2016$ubica_geo)
E2016$upm <- as.character(E2016$upm)

# Rename a column in R
G2016<- data.frame("folioviv" = Gastos2016$folioviv,  "foliohog" = Gastos2016$foliohog, "ubica_geo" = Gastos2016$ubica_geo,"ing_cor" = Gastos2016$ing_cor, "energia" = Gastos2016$energia)
G2016$year<-1:70311
G2016$year<- rep("2016",length(G2016$year))
G2016$folioviv <- as.character(Gastos2016$folioviv)
G2016$ubica_geo <- as.character(Gastos2016$ubica_geo)

#Joined Gastos & Viviendas
A2016 <- left_join(E2016, G2016)

#Unifying 2018 with other tables
E2018<- data.frame("folioviv" = Viv2018$folioviv, "tipo_viv" = Viv2018$tipo_viv, "antiguedad" = Viv2018$antiguedad, "ac" = Viv2018$aire_acond, "calefacc" = Viv2018$calefacc, "ubica_geo" = Viv2018$ubica_geo, "upm" = Viv2018$upm, "tenencia" = Viv2018$tenencia, "num_cuarto" = Viv2018$num_cuarto, "factor" = Viv2018$factor, "tam_loc"=Viv2018$tam_loc)
E2018$year<-1:73405
E2018$year<- rep("2018",length(E2018$year))
E2018$folioviv <- as.character(E2018$folioviv)
E2018$ubica_geo <- as.character(E2018$ubica_geo)
E2018$upm <- as.character(E2018$upm)

# Rename a column in R
G2018<- data.frame("folioviv" = Gastos2018$folioviv,  "foliohog" = Gastos2018$foliohog, "ubica_geo" = Gastos2018$ubica_geo,"ing_cor" = Gastos2018$ing_cor, "energia" = Gastos2018$energia)
G2018$year<-1:74647
G2018$year<- rep("2018",length(G2018$year))
G2018$folioviv <- as.character(Gastos2018$folioviv)
G2018$ubica_geo <- as.character(Gastos2018$ubica_geo)

#Joined Gastos & Viviendas
A2018 <- left_join(E2018, G2018)

AC <- rbind.fill(A2008, A2010, A2012, A2014, A2016, A2018)
```

```{r Table Set-Up, include = F}
#Mutate(ubica_geo = as.character(ubica_geo))
AC %>%
  mutate(tipo_viv = as.factor(tipo_viv), ubica_geo = as.factor(ubica_geo)) %>%
 filter(!tipo_viv == "&")

typeof(AC$ubica_geo)
```

```{r Location Coding, include = F}
createStateFactor <- function(state.var){
  state.var <- factor(state.var, 
                     levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                     labels = c("Aguascalientes", "Baja California", 
                                "Baja California Sur", "Campeche", 
                                "Coahuila", "Colima", "Chiapas", 
                                "Chihuahua", "Mexico City", 
                                "Durango", "Guanajuato", "Guerrero", 
                                "Hidalgo", "Jalisco", "Mexico", 
                                "Michoacan", "Morelos", "Nayarit", 
                                "Nuevo Leon", "Oaxaca", "Puebla", 
                                "Queretaro", "Quintana Roo", 
                                "San Luis Potosi", "Sinaloa", 
                                "Sonora", "Tabasco", "Tamaulipas", 
                                "Tlaxcala", "Veracruz", "Yucatan", 
                                "Zacatecas"))
  return (state.var)
}

createRegionFactor <- function(state.var){
  state.var <- factor(state.var,
                   levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                   labels = c("Templada", "Calida.Ext", "Calida.Ext", "Tropical",
                              "Calida.Ext", "Templada", "Tropical", "Calida.Ext",
                              "Templada","Calida.Ext", "Templada", "Tropical",
                              "Templada", "Templada", "Templada", "Templada",
                              "Templada", "Templada", "Calida.Ext", "Tropical", 
                              "Templada", "Templada", "Tropical", "Templada",
                              "Calida.Ext", "Calida.Ext", "Tropical", "Calida.Ext",
                              "Templada", "Templada", "Tropical", "Templada"))
  return(state.var)
}

createUrbanFactor <- function(urban.code){
  urban.code <- factor(urban.code, 
                     levels = c("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"),
                     labels = c("Urban", "Urban", "Urban", "Urban", "Urban", "Urban", 
                                "Rural","Urban", "Urban", "Urban"))
  return(urban.code)
}  
```

#Location Coding
```{r State & Climate Coding, include = F, eval = T} 
# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
AC$state.code <- substr(AC$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
AC$state <- createStateFactor(AC$state.code)

#Create new variable region into a factor and assign name of region as label
AC$climate <- createRegionFactor(AC$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
AC$urb.code <- substr(AC$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
AC$urban <- createUrbanFactor(AC$urb.code)
```

``` {r More State Coding, include = F}
A2008$state.code <- substr(A2008$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2008$state <- createStateFactor(A2008$state.code)

#Create new variable region into a factor and assign name of region as label
A2008$climate <- createRegionFactor(A2008$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2008$urb.code <- substr(A2008$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2008$urban <- createUrbanFactor(A2008$urb.code)
```

```{r include = F}
A2010$state.code <- substr(A2010$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2010$state <- createStateFactor(A2010$state.code)

#Create new variable region into a factor and assign name of region as label
A2010$climate <- createRegionFactor(A2010$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2010$urb.code <- substr(A2010$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2010$urban <- createUrbanFactor(A2010$urb.code)
```

```{r include = F}
A2012$state.code <- substr(A2012$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2012$state <- createStateFactor(A2012$state.code)

#Create new variable region into a factor and assign name of region as label
A2012$climate <- createRegionFactor(A2012$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2012$urb.code <- substr(A2012$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2012$urban <- createUrbanFactor(A2012$urb.code)
```

```{r include = F}
A2014$state.code <- substr(A2014$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2014$state <- createStateFactor(A2014$state.code)

#Create new variable region into a factor and assign name of region as label
A2014$climate <- createRegionFactor(A2014$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2014$urb.code <- substr(A2014$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2014$urban <- createUrbanFactor(A2014$urb.code)
```

```{r include = F}
A2016$state.code <- substr(A2016$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2016$state <- createStateFactor(A2016$state.code)

#Create new variable region into a factor and assign name of region as label
A2016$climate <- createRegionFactor(A2016$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2016$urb.code <- substr(A2016$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2016$urban <- createUrbanFactor(A2016$urb.code)
```

```{r include = F}
A2018$state.code <- substr(A2018$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2018$state <- createStateFactor(A2018$state.code)

#Create new variable region into a factor and assign name of region as label
A2018$climate <- createRegionFactor(A2018$state.code)

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state
A2018$urb.code <- substr(A2018$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
A2018$urban <- createUrbanFactor(A2018$urb.code)
```

#Renaming Variables
```{r include = F}
A2008$ac <- as.factor(A2008$ac)
A2008$ac <- revalue(A2008$ac, c('1' = '1', '2' = '0'))
A2008$tam_loc <-as.factor(A2008$tam_loc)
A2008$tam_loc <- revalue(A2008$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2008$tam_loc <- factor(A2008$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))

A2010$ac <- as.factor(A2010$ac)
A2010$tam_loc <-as.factor(A2010$tam_loc)
A2010$tam_loc <- revalue(A2010$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2010$tam_loc <- factor(A2010$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))
A2010$ac <- revalue(A2010$ac, c('1' = '1', '2' = '0'))

A2012$ac <- as.factor(A2012$ac)
revalue(A2012$ac, c('1' = '1', '2' = '0'))
A2012$tam_loc <-as.factor(A2012$tam_loc)
A2012$tam_loc <- revalue(A2012$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2012$tam_loc <- factor(A2012$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))

A2014$ac <- as.factor(A2014$ac)
A2014$ac <- revalue(A2014$ac,  c('1' = '1', '2' = '0'))
A2014$tam_loc <-as.factor(A2014$tam_loc)
A2014$tam_loc <- revalue(A2014$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2014$tam_loc <- factor(A2014$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))

A2016$ac <- as.factor(A2016$ac)
A2016$ac <- revalue(A2016$ac, c('1' = '1', '2' = '0'))
A2016$tam_loc <-as.factor(A2016$tam_loc)
A2016$tam_loc <- revalue(A2016$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2016$tam_loc <- factor(A2016$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))

A2018$ac <- as.factor(A2018$ac)
A2018$ac <- revalue(A2018$ac, c('1' = '1', '2' = '0'))
A2018$tam_loc <-as.factor(A2018$tam_loc)
A2018$tam_loc <- revalue(A2018$tam_loc, c('1'= 'large', '2'= 'medium-large', '3' = 'small-medium', '4' = 'small'))
A2018$tam_loc <- factor(A2018$tam_loc, levels= c('small', 'small-medium','medium-large' ,'large'))

summary_AC<-summarytools::descr( AC, weights = AC$factor, style = "rmarkdown")
summary_AC

#Survey design construction to compute statistical error
svd.AC <- svydesign(id =~upm, data = AC, weight=~factor)
svd.AC08 <- svydesign(id =~upm, data = A2008, weight=~factor, na.rm = TRUE)
svd.AC10 <- svydesign(id =~upm, data = A2010, weight=~factor, na.rm = TRUE)
svd.AC12 <- svydesign(id =~upm, data = A2012, weight=~factor, na.rm = TRUE)
svd.AC14 <- svydesign(id =~upm, data = A2014, weight=~factor, na.rm = TRUE)
svd.AC16 <- svydesign(id =~upm, data = A2016, weight=~factor, na.rm = TRUE)
svd.AC18 <- svydesign(id =~upm, data = A2018, weight=~factor, na.rm = TRUE)


```

```{r AC House Type, include = T}
#Determine proportion of households that have air conditioning:
ac.freq.tam08 <-svyby(~ac, by= ~tam_loc, svd.AC08, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot08 <- ggplot(ac.freq.tam08, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Locality Size", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2008") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))

#Determine proportion of households that have air conditioning:
ac.freq.tam10 <-svyby(~ac, by= ~tam_loc, svd.AC10, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot10 <- ggplot(ac.freq.tam10, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Locality Size", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2010") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))

    #Determine proportion of households that have air conditioning:
ac.freq.tam12 <-svyby(~ac, by= ~tam_loc, svd.AC12, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot12 <- ggplot(ac.freq.tam12, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Housing Type", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2012") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))

    
      #Determine proportion of households that have air conditioning:
ac.freq.tam14 <-svyby(~ac, by= ~tam_loc, svd.AC14, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot14 <- ggplot(ac.freq.tam14, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Locality Size", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2014") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))

 
    #Determine proportion of households that have air conditioning:
ac.freq.tam16 <-svyby(~ac, by= ~tam_loc, svd.AC16, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot16 <- ggplot(ac.freq.tam16, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Locality Size", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2016") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))
     
      #Determine proportion of households that have air conditioning:
ac.freq.tam18 <-svyby(~ac, by= ~tam_loc, svd.AC18, svymean,
                           na.rm=TRUE)

    #Make Bar Plot
     air.freq.tam.plot18 <- ggplot(ac.freq.tam18, aes(x = tam_loc, y = ac1, fill=ac1)) +
          geom_bar(stat="identity") +
          labs(x = "Locality Size", y = "Percentage", fill = "AC Adoption Rate", title = "AC Adoption by Locality Size 2018") +
        scale_fill_viridis_c(option = "viridis", begin = 0.2, end = 0.8) +
          scale_x_discrete(name= "Locality Size")+
        scale_y_continuous(name = "", labels = scales::percent, limits = c(0,.3)) +
       theme(axis.text.x = element_text(angle = 0))

air.freq.tam.plot08
air.freq.tam.plot10
air.freq.tam.plot12
air.freq.tam.plot14
air.freq.tam.plot16
air.freq.tam.plot18
```