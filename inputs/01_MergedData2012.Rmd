---
title: "Merge 2012 Data"
author: "Jessica Siegel and Amanda Ullman"
date: "10/3/2019"
output: html_document
---

```{r setup, include=FALSE}
rm(list=ls())


#Set WD
setwd("~/Desktop/MP/mx_electricity_residential/inputs")
#Load Packages
if (!require("pacman")) install.packages("pacman")
  pacman::p_load(pacman, party, psych, rio, tidyverse, ggplot2, magrittr, plyr, knitr, dplyr, ggplot2,bit64, survey, summarytools, ENmisc, naniar)

opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
```

#Load 2012
```{r load_datdf14, include = FALSE}
path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2012/"
dwellings<-read_csv(paste0(path.data, "2012_enigh_viviendas.csv"))
household<-read_csv(paste0(path.data, "2012_enigh_hogares.csv"))
household.conc<-read_csv(paste0(path.data, "2012_enigh_concentradohogares.csv"))

household<-household %>%
  select("folioviv", "foliohog", "num_tv","anio_tv", "num_licua",	"anio_licua", "num_tosta",	"anio_tosta", "num_micro", "anio_micro","num_refri","anio_refri","num_lavad","num_compu","anio_compu", "anio_lavad")


household.conc <-household.conc %>%
  select("folioviv","foliohog", "tam_loc", "est_socio", "est_dis", "upm", "factor_hog", "clase_hog", "edad_jefe", "educa_jefe", "tot_integ","p65mas","ing_cor", "energia", "ubica_geo")

dwellings<- dwellings %>%
  select("folioviv", "tipo_viv", "mat_pared", "mat_pisos","mat_techos", "antiguedad","num_cuarto","disp_elect", "combustible", "tenencia", "renta","estim_pago", "lavadero", "medidor_luz", "calentador", "aire_acond", "calefacc", "tot_resid", "tot_hog", "num_focos", "tenencia")

```


```{r add_values}
colnames(dwellings)[colnames(dwellings)=="num_focos"] <-"focos_tot"

dwellings$rent <- ifelse(is.na(dwellings$renta), dwellings$estim_pago, dwellings$renta)

household.conc<- as_tibble(household.conc) 

household.conc%>%
 mutate(ubica_geo = as.character(ubica_geo))

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state:
household.conc$state.code <- substr(household.conc$ubica_geo, 1, 2)

household.conc$state <- factor(household.conc$state.code, 
                     levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                     labels = c("Aguascalientes", "Baja California", 
                                "Baja California Sur", "Campeche", 
                                "Coahuila", "Colima", "Chiapas", 
                                "Chihuahua", "Mexico City", 
                                "Durango", "Guanajuato", "Guerrero", 
                                "Hidalgo", "Jalisco", "Mexico", 
                                "Michoacan", "Morelos", "Nayarit", 
                                "Nuevo Leon", "Oaxaca", "Puebla", 
                                "Queretaro", "Quintana Roo", 
                                "San Luis Potosi", "Sinaloa", 
                                "Sonora", "Tabasco", "Tamaulipas", 
                                "Tlaxcala", "Veracruz", "Yucatan", 
                                "Zacatecas"))

household.conc$climate <- factor(household.conc$state.code,
                   levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                   labels = c("Temperate", "Arid", "Arid", "Tropical",
                              "Arid", "Temperate", "Tropical", "Arid",
                              "Temperate","Arid", "Temperate", "Tropical",
                              "Temperate", "Temperate", "Temperate", "Temperate",
                              "Temperate", "Temperate", "Arid", "Tropical", 
                              "Temperate", "Temperate", "Tropical", "Temperate",
                              "Arid", "Arid", "Tropical", "Arid",
                              "Temperate", "Tropical", "Tropical", "Temperate"))

```

Renaming:
```{r merge2016}
df12<- merge(household, household.conc, by=c("folioviv","foliohog"))

df12<-merge(dwellings, df12, by="folioviv")

df12$ing_cor_USD<- df12$ing_cor*0.052
df12$energia_USD<-df12$energia*0.052

df12$rent_USD<- df12$rent*0.052


df12$calentador[df12$calentador==2]<-0
df12$aire_acond[df12$aire_acond==2]<-0
df12$calefacc[df12$calefacc==2]<-0


df12$energia[df12$energia <=0] <- NA

count(df12$energia<=0)

df12$ing_cor[df12$ing_cor<=0] <- NA
df12$energia_USD[df12$energia_USD <=0] <-NA
df12$ing_cor_USD[df12$ing_cor_USD<=0] <-NA
df12$energia_USD[df12$energia_USD <=0] <- NA
df12$rent[df12$rent<=0] <-NA
df12$rent_USD[df12$rent_USD<=0] <-NA
df12$anio_tv<-0
df12$anio_tv[df12$anio_tv<=0] <-NA

sum(is.na(df12$energia))
sum(is.na(df12$ing_cor))
sum(is.na(df12$rent_USD))

df12$energia_prop<-df12$energia/df12$ing_cor

df12$year<-1:9002
df12$year<- rep("2012",length(df12$year))

```

```{r rename}

df12<-df12%>%rename(c('aire_acond'='ac'))
df12<-df12%>%rename(c('calefacc'='heat'))
df12 <- df12 %>% rename(c('factor_hog' = 'factor'))

df12$tipo_viv <- as.factor(df12$tipo_viv)
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="&"] <- "other"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="1"] <- "house"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="2"] <- "apartment"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="3"] <- "other"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="4"] <- "other"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="5"] <- "other"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="6"] <- "other"
levels(df12$tipo_viv)[levels(df12$tipo_viv)=="7"] <- "other"


df12$educa <-as.factor(df12$educa_jefe)
levels(df12$educa)[levels(df12$educa)=="01"] <- "elementary incomplete(1,2,3)"
levels(df12$educa)[levels(df12$educa)=="02"] <- "elementary incomplete(1,2,3)"
levels(df12$educa)[levels(df12$educa)=="03"] <- "elementary incomplete(1,2,3)"
levels(df12$educa)[levels(df12$educa)=="04"] <- "elementary complete(4,5)"
levels(df12$educa)[levels(df12$educa)=="05"] <- "elementary complete(4,5)"
levels(df12$educa)[levels(df12$educa)=="06"] <- "middle school complete(6,7)"
levels(df12$educa)[levels(df12$educa)=="07"] <- "middle school complete(6,7)"
levels(df12$educa)[levels(df12$educa)=="08"] <- "high school complete(8,9)"
levels(df12$educa)[levels(df12$educa)=="09"] <- "high school complete(8,9)"
levels(df12$educa)[levels(df12$educa)=="10"] <- "undergrad or higher(10,11)"
levels(df12$educa)[levels(df12$educa)=="11"] <- "undergrad or higher(10,11)"

df12$city <- as.factor(df12$folioviv)
levels(df12$city)[levels(df12$city)=="9014"] <- "Mexico City"
levels(df12$city)[levels(df12$city)=="8037"] <- "Ciudad Juarez"
levels(df12$city)[levels(df12$city)=="14039"] <- "Guadalajara"
levels(df12$city)[levels(df12$city)=="31050"] <- "Merida"
levels(df12$city)[levels(df12$city)=="19039"] <- "Monterrey"
levels(df12$city)[levels(df12$city)=="9014"] <- "Puebla"
levels(df12$city)[levels(df12$city)=="2004"] <- "Tijuana"
levels(df12$city)[levels(df12$city)=="7101"] <- "Tuxtla Gutierrez"
levels(df12$city)[levels(df12$city)=="30193"] <- "Veracruz"


df12$refri<-df12$num_refri
df12$refri[df12$refri>0]=1

df12$micro<-df12$num_micro
df12$micro[df12$micro>0]=1

df12$est_socio<-as.factor(df12$est_socio)
df12$tam_loc<-as.factor(df12$tam_loc)



df12$est_socio<-revalue(df12$est_socio, c('1' = 'low', '2' = 'medium-low', '3' = 'medium-high', '4' = 'high'))

df12$urban <- as.factor(df12$tam_loc)
levels(df12$urban)[levels(df12$urban)=="4"] <- "Rural"
levels(df12$urban)[levels(df12$urban)=="3"] <- "Urban"
levels(df12$urban)[levels(df12$urban)=="2"] <- "Urban"
levels(df12$urban)[levels(df12$urban)=="1"] <- "Urban"

df12$tam_loc <- revalue(df12$tam_loc, c('1'= '>=100K', '2'= '15K-100K', '3' = '2.5K-15K', '4' = '<2.5K'))




df12$ac <- as.factor(df12$ac)
df12$heat <- as.factor(df12$heat)

df12$houseage <- cut(df12$antiguedad,
                 breaks = c(0,3,7,10,20,30,50,Inf),
                 labels = c("less than 3", "3 to 6", "7 to 10", "11 to 20", "21 to 30", "31 to 50", "50+"))

df12$num_room <- cut(df12$num_cuarto,
                 breaks = c(0,1,4,8,10,Inf),
                 labels = c("1 room", "2-4 rooms", "5-7 rooms", "8-10 rooms","10+ rooms")) 

df12$size <- cut(df12$num_cuarto,
                 breaks = c(0,4,8, Inf),
                 labels = c("Small", "Medium","Large"))

levels(df12$size)

df12$est_socio <- factor (df12$est_socio, levels = c('low', 'medium-low', 'medium-high', 'high'))

df12$tam_loc <- factor(df12$tam_loc, levels = c('<2.5K', '2.5K-15K', '15K-100K', '>=100K'))

df12$income <- cut(df12$ing_cor_USD,
                 breaks = c(0,950,1600,2300,Inf),
                 labels = c("Low", "Med-Low", "Med-High", "High"))

count(df12$ac[is.na(df12$ac)])
count(df12$heat[is.na(df12$heat)])
count(df12$refri[is.na(df12$refri)])
count(df12$micro[is.na(df12$micro)])
count(df12$houseage[is.na(df12$houseage)])
count(df12$num_room[is.na(df12$num_room)])
count(df12$focos_tot[is.na(df12$focos_tot)])
count(df12$num_refri[is.na(df12$num_refri)])
count(df12$num_lavad[is.na(df12$num_lavad)])
count(df12$tam_loc[is.na(df12$tam_loc)])
```

```{r}
df12$ac[is.na(df12$ac)] <- 0
df12$heat[is.na(df12$heat)] <- 0
df12$micro[is.na(df12$micro)] <- 0
df12$refri[is.na(df12$refri)] <- 0


df12$num_lavad[is.na(df12$num_lavad)] <- 0
df12$num_micro[is.na(df12$num_micro)] <- 0
df12$num_tosta[is.na(df12$num_tosta)] <- 0
df12$num_licua[is.na(df12$num_licua)] <- 0
df12$num_refri[is.na(df12$num_refri)] <- 0
df12$focos_tot[is.na(df12$focos_tot)] <- 0


write.csv(df12, file = "df12.csv",row.names=FALSE)
```