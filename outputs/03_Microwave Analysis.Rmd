---
title: "Master Microwave"
author: "Jess Siegel"
date: "01/15/2020"
output: html_document
---

```{r setup, include=FALSE}
rm(list=ls())


#Set WD
setwd("~/Desktop/MP/mx_electricity_residential/outputs")
#Load Packages
if (!require("pacman")) install.packages("pacman")
  pacman::p_load(pacman, party, psych, rio, BiocManager, tidyverse, ggplot2, magrittr, plyr, knitr, dplyr, ggplot2, survey, summarytools, ENmisc, dplyr, xtable, formattable, reshape, reshape2, tables)

opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
```

```{r upload, include= FALSE}
f18<-read_csv("../inputs/df18.csv")
f16<-read_csv("../inputs/df16.csv")
f14<-read_csv("../inputs/df14.csv")
f12<-read_csv("../inputs/df12.csv")

f18<-f18 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad", "num_micro", "focos_tot","num_compu", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))
  
f16<-f16 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad", "num_micro", "focos_tot", "num_compu","state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f14<-f14 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad", "num_micro", "focos_tot", "num_compu","state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f12<-f12 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad", "num_micro", "focos_tot","num_compu", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))


f18$micro<-f18$num_micro
f18$micro[f18$micro>0]=1

f16$micro<-f16$num_micro
f16$micro[f16$micro>0]=1

f14$micro<-f14$num_micro
f14$micro[f14$micro>0]=1

f12$micro<-f12$num_micro
f12$micro[f12$micro>0]=1

#Combine Data Sets
fnew<-rbind(f12, f14, f16, f18)

fnew$micro<-fnew$num_micro
fnew$micro[fnew$micro>0]=1

#f14$micro<-f14$num_micro
#f14$micro[f14$micro>0]=1

```


#Summary of relevant data
```{r survey_design, include=FALSE, echo=TRUE}

#Summary Statistics for each year

summary_1<-summarytools::descr( fnew, weights = fnew$factor, strata=~fnew$est_dis, style = "rmarkdown")
summary_2018<-summarytools::descr( f18, weights = f18$factor, strata=~fnew$est_dis, style = "rmarkdown")
summary_2016<-summarytools::descr( f16, weights = f16$factor, strata=~fnew$est_dis, style = "rmarkdown")
summary_2014<-summarytools::descr( f14, weights = f14$factor, strata=~fnew$est_dis, style = "rmarkdown")
summary_fnew<-summarytools::descr( fnew, weights = fnew$factor, strata=~fnew$est_dis, style = "rmarkdown")

summary_2018
summary_2016
summary_2014
summary_fnew


#Create Survey Design for additional Analysis of Survey Statistics

#creating a design for all years under analysis
svd.fnew <- svydesign(id =~upm, data = fnew, weight=~factor, strata=fnew$est_dis, nest=TRUE)

#Create Survey Design for each indidual year
svd.f12 <- svydesign(id =~upm, data = f12, weight=~factor, na.rm = TRUE)
svd.f14 <- svydesign(id =~upm, data = f14, weight=~factor, na.rm = TRUE)
svd.f16 <- svydesign(id =~upm, data = f16, weight=~factor, na.rm = TRUE)
svd.f18 <- svydesign(id =~upm, data = f18, weight=~factor, na.rm = TRUE)
```

#Adoption of Microwaves in Mexico

#of microwaves in each household Graph
```{r micro_ALL, include=TRUE, echo =FALSE}

mean.prop.year <- svyby(~num_micro, 
                           by=~year, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)
ggplot(data=mean.prop.year, 
       aes(x=year, y=num_micro, 
           label = rownames(num_micro) ))+
  geom_bar(stat="identity", fill= "steelblue",  position = "dodge", width=0.5) + 
  geom_errorbar(aes(ymin=num_micro-se, ymax=num_micro+se),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
  geom_text(aes(label=round(num_micro,3)), vjust=3.5, color="white", size=3.5) +
  scale_x_discrete(name= "Year", limits=c(2012,2014, 2016, 2018))+
  ggtitle("Average # of Microwaves per household") +
  theme_bw()
```

##Proportion of households with microwave
```{r microbulbs_ALL, include=TRUE, echo =FALSE}

mean.micro.year <- svyby(~micro, 
                           by=~year, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

tbl.micro.year <- svytable(~micro+year, design = svd.fnew) %>%
  as.data.frame()

tbl.micro.year

ggplot(data=mean.micro.year, 
       aes(x=year, y=micro, 
           label = rownames(micro) ))+
  geom_bar(stat="identity", fill= "steelblue",  position = "dodge", width=0.5) + 
  geom_errorbar(aes(ymin=micro-se, ymax=micro+se),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
  #geom_text(aes(label=c(22.87,28.16, 29.88)), vjust=3.5, color="white", size=3.5) +
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018))+
  theme_bw()+ylab("Households with Energy Saving Bulbs")+  labs(title = "Households with Energy saving bulbs in Mexico", caption ="Numbers in millions")

###note numbers above in the bar were pulled rom info below.
#Proportion of bulbs that are energy saving within those households.
```

```{r f_micro_table, include = TRUE}

#Get the mean values of enrgy saving bulb adoption, SE, and frequency for year 
GetAnnualValues <- function(year.value, survey.design)
{
  df.year.micro <- svymean(~interaction(micro), 
                           design = survey.design, na.rm = TRUE) %>% 
                   as.data.frame()
  df.year.micro <- cbind(svytable(~micro, design = survey.design) %>% 
                           as.data.frame(), df.year.micro)
  df.year.micro <- add_column(df.year.micro, year = year.value, .before = 1)
  return(df.year.micro)
}

#Compute the mean values and SE of energy saving bulbs for 2014
df.freq.micro <- svymean(~interaction(micro), design = svd.f14, na.rm = TRUE) %>% as.data.frame() 

# Calculate the frequency values and binding the dataframe with 
#the previous df that has the mean values 
df.freq.micro <- cbind(svytable(~micro, design = svd.f14) %>% 
                      as.data.frame(), df.freq.micro)
df.freq.micro <- add_column(df.freq.micro, year = 2014, .before = 1)


# Binding the values of the previous year with the year year.value
df.freq.micro<- rbind(df.freq.micro, GetAnnualValues(2016,svd.f16))
df.freq.micro<- rbind(df.freq.micro, GetAnnualValues(2018,svd.f18))

#Convert lightbulb variable to factor
df.freq.micro$micro <- factor(df.freq.micro$micro, levels = c(0, 1), labels = c('no','yes'))

#Replacing rownames
rownames(df.freq.micro) <- paste0('micro_', df.freq.micro$year, '_', df.freq.micro$micro )
a<-as.data.frame(df.freq.micro)
a

```


```{r plt-ac-perc}
#Plots chart with "yes" and "no" answers for percent of population that has adopted energy saving bulbs
plt.micro.perc <- ggplot(df.freq.micro, aes(x = year , y = mean, 
                                          ymin = (mean-2*SE), 
                                          ymax = (mean+2*SE),  
                                          fill = micro)) +
  geom_bar(stat="identity", position = "dodge") +
  geom_errorbar(position=position_dodge(width=1.8), width=1.1) + 
  scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) +
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018)) +
  scale_y_continuous(name = "", labels = scales::percent) +
  geom_text(aes(label=round(Freq/1000000, 2)), vjust=2.6, color="white", 
            position = position_dodge(1.6), size=3) +
  labs(title = "Adoption of Energy saving bulbs in Mexico", caption ="Numbers in millions")
plt.micro.perc
```

```{r propbulbs_socio, include=TRUE}

#of microwaves in each home
mean.total<- svyby(~num_micro, by=~year+est_socio, svd.fnew, svymean, na.rm=TRUE)

mean.prop.year <- svyby(~num_micro, 
                           by=~year+est_socio, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

#Plots proportion of bulbs that are energy savings for all 3 years that have adopted energy saving bulbs by socioeconomic status.

q<-ggplot(mean.total,aes(x=year,y=num_micro, fill=factor(est_socio)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Socioeconomic",
                      breaks=c(1, 2, 3, 4),
                      labels=c("low","low-medium", "high-medium","high"))+ 
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018)) +
  geom_errorbar(position=position_dodge(width=.9), width=.5) + aes(ymin=num_micro-se, ymax=num_micro+se)+ 
  geom_text(aes(label=round(num_micro, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "# of Microwaves in Mexico by Socioeconomic Status", caption ="Number in percent", y="number per household")
```

The charts below summarize the frequency and proportion of households that have adopted energy saving bulbs, by year & socioeconomic status. These numbers are followed by a graphical representation of the data.

```{r microbulbs_socio, include=TRUE, echo = FALSE}
#proportion of households with energy saving bulbs
mean.total<- svyby(~micro, by=~year+est_socio, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~micro+year+est_socio, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$micro == 1, ]

socio_micro<- merge(freq2, mean.total2, by=c("year", "est_socio"))%>%select("est_socio", "year", "micro.y", "se", "Freq")%>%rename(c('micro.y'='adopt.rate'))

socio_micro

q<-ggplot(mean.total,aes(x=year,y=micro, fill=factor(est_socio)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Socioeconomic",
                      breaks=c(1, 2, 3, 4),
                      labels=c("low","low-medium", "high-medium","high"))+ 
  scale_x_discrete(name= "Year", limits=c(2012, 2014, 2016, 2018)) +
  geom_errorbar(position=position_dodge(width=.9), width=.5) + aes(ymin=micro-se, ymax=micro+se)+ 
  geom_text(aes(label=round(micro, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households with Microwaves in Mexico by Socioeconomic Status", caption ="Number in percent", y="Proportion of Households")
```

```{r micro_urban, include=TRUE}

#proportion of households with energy saving bulbs
mean.total<- svyby(~micro, by=~year+urban, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~micro+year+urban, design = svd.fnew) %>%as.data.frame()

#select only households with energy saving bulbs
freq2<-freq[freq$micro == 1, ]

urban_micro<- merge(freq2, mean.total2, by=c("year", "urban"))%>%select("urban", "year", "micro.y", "se", "Freq")%>%rename(c('micro.y'='adopt.rate'))

q<-ggplot(mean.total,aes(x=year,y=micro, fill=factor(urban)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Urban")+ 
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018)) +
  geom_text(aes(label=round(micro, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households Adoption of Energy Saving Bulbs in Urban v. Rural Households", caption ="Number in percent", y="Proportion of Households")

urban_micro

```

```{r micro_tipo_viv, include=TRUE}

#proportion of households with energy saving bulbs
mean.total <- svyby(~micro, 
                           by=~year+tipo_viv, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

mean.total2<-mean.total %>%as.data.frame()
freq<- svytable(~micro+year+tipo_viv, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$micro == 1, ]

tipo_viv_micro<- merge(freq2, mean.total2, by=c("year", "tipo_viv"))%>%select("tipo_viv", "year", "micro.y", "se", "Freq")%>%rename(c('micro.y'='adopt.rate'))

tipo_viv_micro

q<-ggplot(mean.total,aes(x=year,y=micro, fill=factor(tipo_viv)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Housing Type",
                      breaks=c("house", "apartment", "other"),
                      labels=c("Houses","Apartments", "Other"))+ 
  scale_x_discrete(name= "Year", limits=c(2012, 2014, 2016, 2018)) +
  geom_text(aes(label=round(micro, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households Adoption of Energy Saving Bulbs in Mexico by Housing Type", caption ="Number in percent", y="Proportion of Households")

tipo_freq<- svytable(~micro+year+tipo_viv, design = svd.fnew) %>%as.data.frame()
```

The charts below shows the household adoption rate of microwaves given socioeconomic status and locality size in each year:
##2014
```{r socio_tam2014, include=TRUE, echo=FALSE}

mean.total<- svyby(~micro, by=~tam_loc + est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)

mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +tam_loc +year, design = svd.f14) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_tam<- merge(freq2, mean.total2, by=c("est_socio", "tam_loc")) %>% select("year", "est_socio", "tam_loc", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','tam_loc'='Locality_Size'))

socio_tam$adopt.rate<-socio_tam$adopt.rate

socio_tam
```


##2018
```{r socio_tipo2018, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~tipo_viv+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +tipo_viv, design = svd.f18) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_tipo<- merge(freq2, mean.total2, by=c("est_socio", "tipo_viv")) %>% select("est_socio", "tipo_viv", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','tipo_viv'='Locality_Size'))

socio_tipo$adopt.rate<-socio_tipo$adopt.rate*100

socio_tipo #dataframe/with all data

tbl.socio_tipo = dcast(socio_tipo, est_socio ~ tipo_viv, value.var = "adopt.rate")
tbl.socio_tipo 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_tipo, id.vars = c("est_socio", "tipo_viv"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ tipo_viv + variable, value.var = "value")
basic_summ_t2
#socio_tipo%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_tipo), type = "html", file = "socio_tipo.html")
#file.show("socio_tipo.html")
```

The chart below shows the household adoption rate of lightbulbs given socioeconomic status and urban/rural status
#2014
```{r socio_urb, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~urban+est_socio, svd.f14, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +urban, design = svd.f14) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_urb<- merge(freq2, mean.total2, by=c("est_socio", "urban")) %>% select("est_socio", "urban", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','urban'='Locality_Size'))

socio_urb$adopt.rate<-socio_urb$adopt.rate*100

socio_urb #dataframe/with all data

tbl.socio_urb = dcast(socio_urb, est_socio ~ urban, value.var = "adopt.rate")
tbl.socio_urb 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_urb, id.vars = c("est_socio", "urban"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ urban + variable, value.var = "value")
basic_summ_t2
#socio_urb%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_urb), type = "html", file = "socio_urb.html")
#file.show("socio_urb.html")
```
```{r socio_urb2016, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~urban+est_socio, svd.f16, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +urban, design = svd.f16) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_urb<- merge(freq2, mean.total2, by=c("est_socio", "urban")) %>% select("est_socio", "urban", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','urban'='Locality_Size'))

socio_urb$adopt.rate<-socio_urb$adopt.rate*100

socio_urb #dataframe/with all data

tbl.socio_urb = dcast(socio_urb, est_socio ~ urban, value.var = "adopt.rate")
tbl.socio_urb 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_urb, id.vars = c("est_socio", "urban"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ urban + variable, value.var = "value")
basic_summ_t2
#socio_urb%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_urb), type = "html", file = "socio_urb.html")
#file.show("socio_urb.html")
```

```{r socio_urb2018, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~urban+est_socio, svd.f18, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +urban, design = svd.f18) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_urb<- merge(freq2, mean.total2, by=c("est_socio", "urban")) %>% select("est_socio", "urban", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','urban'='Locality_Size'))

socio_urb$adopt.rate<-socio_urb$adopt.rate*100

socio_urb #dataframe/with all data

tbl.socio_urb = dcast(socio_urb, est_socio ~ urban, value.var = "adopt.rate")
tbl.socio_urb 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_urb, id.vars = c("est_socio", "urban"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ urban + variable, value.var = "value")
basic_summ_t2
#socio_urb%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_urb), type = "html", file = "socio_urb.html")
#file.show("socio_urb.html")
```

The chart below shows the household adoption rate of lightbulbs given socioeconomic status and urban/rural status.
#2014
```{r socio_clima2014, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~climate+est_socio, svd.f14, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +climate, design = svd.f14) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_clima<- merge(freq2, mean.total2, by=c("est_socio", "climate")) %>% select("est_socio", "climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','climate'='Locality_Size'))

socio_clima$adopt.rate<-socio_clima$adopt.rate*100

socio_clima #dataframe/with all data

tbl.socio_clima = dcast(socio_clima, est_socio ~ climate, value.var = "adopt.rate")
tbl.socio_clima 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_clima, id.vars = c("est_socio", "climate"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ climate + variable, value.var = "value")
basic_summ_t2
#socio_clima%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_clima), type = "html", file = "socio_clima.html")
#file.show("socio_clima.html")
```
#2016
```{r socio_clima2016, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~climate+est_socio, svd.f16, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +climate, design = svd.f16) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_clima<- merge(freq2, mean.total2, by=c("est_socio", "climate")) %>% select("est_socio", "climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','climate'='Locality_Size'))

socio_clima$adopt.rate<-socio_clima$adopt.rate*100

socio_clima #dataframe/with all data

tbl.socio_clima = dcast(socio_clima, est_socio ~ climate, value.var = "adopt.rate")
tbl.socio_clima 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_clima, id.vars = c("est_socio", "climate"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ climate + variable, value.var = "value")
basic_summ_t2
#socio_clima%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_clima), type = "html", file = "socio_clima.html")
#file.show("socio_clima.html")
```
#2018
```{r socio_clima2018, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~climate+est_socio, svd.f18, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +climate, design = svd.f18) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_clima<- merge(freq2, mean.total2, by=c("est_socio", "climate")) %>% select("est_socio", "climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','climate'='Locality_Size'))

socio_clima$adopt.rate<-socio_clima$adopt.rate*100

socio_clima #dataframe/with all data

tbl.socio_clima = dcast(socio_clima, est_socio ~ climate, value.var = "adopt.rate")
tbl.socio_clima 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_clima, id.vars = c("est_socio", "climate"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ climate + variable, value.var = "value")
basic_summ_t2
#socio_clima%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_clima), type = "html", file = "socio_clima.html")
#file.show("socio_clima.html")
```

The following charts show the adoption rate of energy saving lightbulbs given the size of the house and the socioeconomic status of the home
##2014
```{r socio_urb_clima14, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~num_room+est_socio, svd.f14, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +num_room, design = svd.f14) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_size<- merge(freq2, mean.total2, by=c("est_socio", "num_room")) %>% select("est_socio", "num_room", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_size$adopt.rate<-socio_size$adopt.rate*100

socio_size #dataframe/with all data

tbl.socio_size = dcast(socio_size, est_socio ~ num_room, value.var = "adopt.rate")
tbl.socio_size 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_size, id.vars = c("est_socio", "num_room"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ num_room + variable, value.var = "value")
basic_summ_t2
#socio_size%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_size), type = "html", file = "socio_size.html")
#file.show("socio_size.html")
```
##2016
```{r socio_urb_clima16, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~num_room+est_socio, svd.f16, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +num_room, design = svd.f16) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_size<- merge(freq2, mean.total2, by=c("est_socio", "num_room")) %>% select("est_socio", "num_room", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_size$adopt.rate<-socio_size$adopt.rate*100

socio_size #dataframe/with all data

tbl.socio_size = dcast(socio_size, est_socio ~ num_room, value.var = "adopt.rate")
tbl.socio_size 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_size, id.vars = c("est_socio", "num_room"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ num_room + variable, value.var = "value")
basic_summ_t2
#socio_size%>%formattable()
##Easy formatting for transfer into excel
#print(xtable(socio_size), type = "html", file = "socio_size.html")
#file.show("socio_size.html")
```
##2018
```{r socio_urb_clima18, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~num_room+est_socio, svd.f18, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +num_room, design = svd.f18) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_size<- merge(freq2, mean.total2, by=c("est_socio", "num_room")) %>% select("est_socio", "num_room", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_size$adopt.rate<-socio_size$adopt.rate*100

socio_size #dataframe/with all data

tbl.socio_size = dcast(socio_size, est_socio ~ num_room, value.var = "adopt.rate")
tbl.socio_size 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_size, id.vars = c("est_socio", "num_room"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ num_room + variable, value.var = "value")
basic_summ_t2
socio_size%>%formattable()
#Easy formatting for transfer into excel
print(xtable(socio_size), type = "html", file = "socio_size.html")
file.show("socio_size.html")
```

##2018
```{r socio_sizenew, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~num_room+est_socio+ year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +num_room + year, design = svd.f16) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_size<- merge(freq2, mean.total2, by=c("est_socio", "num_room", "year")) %>% select("year", "est_socio", "num_room", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_size$adopt.rate<-socio_size$adopt.rate*100

socio_size #dataframe/with all data

tbl.socio_size = dcast(socio_size, est_socio ~ num_room, value.var = "adopt.rate")
tbl.socio_size 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_size, id.vars = c("year", "est_socio", "num_room"), 
                                 measure.vars = c("adopt.rate", "se", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ num_room + year+ variable, value.var = "value")
basic_summ_t2
socio_size%>%formattable()

write.csv(socio_size,"~/Desktop/MP/mx_electricity_residential/outputs/socio_size.csv", row.names = FALSE)
```

#4-way Table

##All Years
```{r socio_urban_climate18, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~est_socio + urban + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +urban +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_urban_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "urban", "climate")) %>% select("year", "est_socio", "urban","climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','urban'='Locality_Size'))

socio_urban_climate$adopt.rate<-socio_urban_climate$adopt.rate

#This is what you are exporting to CSV
socio_urban_climate #dataframe/with all data

tbl.socio_urban_climate = dcast(socio_urban_climate, est_socio ~ urban+climate, value.var = "adopt.rate")
tbl.socio_urban_climate 

# first melt the data frame to put all the metrics in a single column
basic_summ_t2 = melt(socio_urban_climate, id.vars = c("est_socio", "urban", "climate", "year"), 
                                 measure.vars = c("adopt.rate", "se","year", "Freq"))
basic_summ_t2 = dcast(basic_summ_t2, est_socio ~ urban + climate + variable, value.var = "value")
basic_summ_t2
socio_urban_climate%>%formattable()

write.csv(socio_urban_climate,"~/Desktop/MP/mx_electricity_residential/outputs/socio_urban_climate_all.csv", row.names = FALSE)
```
```{r socio_viv_climateall, include=TRUE, echo=FALSE}
mean.total<- svyby(~micro, by=~est_socio + tipo_viv + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +tipo_viv +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_tipo_viv_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "tipo_viv", "climate")) %>% select("year", "est_socio", "tipo_viv","climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','tipo_viv'='Locality_Size'))

socio_tipo_viv_climate$adopt.rate<-socio_tipo_viv_climate$adopt.rate

socio_tipo_viv_climate #dataframe/with all data

write.csv(socio_tipo_viv_climate,"~/Desktop/MP/mx_electricity_residential/outputs/socio_tipo_viv_climate_all.csv", row.names = FALSE)
```

```{r socio_viv_climateall, include=TRUE, echo=FALSE}

mean.total<- svyby(~micro, by=~est_socio + num_room + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adopt.rate'))

freq<- svytable(~micro + est_socio +num_room +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_num_room_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "num_room", "climate")) %>% select("year", "est_socio", "num_room","climate", "adopt.rate", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_num_room_climate$adopt.rate<-socio_num_room_climate$adopt.rate

socio_num_room_climate #dataframe/with all data

write.csv(socio_num_room_climate,"~/Desktop/MP/mx_electricity_residential/outputs/socio_num_room_climate_all.csv", row.names = FALSE)
```
```{r socio_tam_loc_climateall, include=TRUE, echo=FALSE}

mean.total<- svyby(~micro, by=~est_socio + tam_loc + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('micro'='adoption'))

freq<- svytable(~micro + est_socio +tam_loc +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$micro == 1, ]%>%as.data.frame() #selects just those that have been adopted

#Merge into one table:
socio_tam_loc_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "tam_loc", "climate")) %>% select("year", "est_socio", "tam_loc","climate", "adoption", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','tam_loc'='Locality_Size'))

socio_tam_loc_climate$adoption<-socio_tam_loc_climate$adoption

socio_tam_loc_climate #dataframe/with all data

write.csv(socio_tam_loc_climate,"~/Desktop/MP/mx_electricity_residential/outputs/socio_tam_loc_climate_all.csv", row.names = FALSE)
```


