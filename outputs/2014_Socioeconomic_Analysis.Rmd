
```{r Setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r Clean Memory/ Auto Load, echo = F}
#Clean Memory
rm(list=ls())

#garbage collector
gc()

#Make packages auto load
if(!require("pacman")) install.packages("pacman")
```

```{r Packages, echo = F}
#load contribute packages with pacman
pacman::p_load(pacman, rio, tidyverse, bit64,forcats,gridExtra,lubridate,cowplot,summarytools,RColorBrewer,survey, scales, ENmisc, ggrepel, lmerTest, weights)
```

```{r Table Set-Up, include=FALSE}

#Source: https://www.inegi.org.mx/programas/enigh/nc/2014/default.html#Microdatos



path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2014/"

E2014 <-read_csv(paste0(path.data, "2014_enigh_viviendas.csv"))

Gastos <-read_csv(paste0(path.data, "2014_ENIGH_concentradohogar.csv"))

#Make Special Table
viv <- as_tibble(E2014) 
gasto <-as_tibble(Gastos)

viv2<- as.data.frame(E2014)
gasto2 <-as.data.frame(Gastos)
ls <- left_join(viv2, gasto2)
  

#Mutate(ubica_geo = as.character(ubica_geo))
ls <- ls %>%
  mutate(ubica_geo = as.character(ubica_geo), tipo_viv = as.factor(tipo_viv)) %>%
 filter(!tipo_viv == "&")
```

#Location Coding
```{r, State & Climate Coding, include = F, eval = T} 

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state

ls$state.code <- substr(ls$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
ls$state <- factor(ls$state.code, 
                     levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                     labels = c("Aguascalientes", "Baja California", 
                                "Baja California Sur", "Campeche", 
                                "Coahuila", "Colima", "Chiapas", 
                                "Chihuahua", "Mexico City", 
                                "Durango", "Guanajuato", "Guerrero", 
                                "Hidalgo", "Jalisco", "Mexico", 
                                "Michoacan", "Morelos", "Nayarit", 
                                "Nuevo Leon", "Oaxaca", "Puebla", 
                                "Queretaro", "Quintana Roo", 
                                "San Luis Potosi", "Sinaloa", 
                                "Sonora", "Tabasco", "Tamaulipas", 
                                "Tlaxcala", "Veracruz", "Yucatan", 
                                "Zacatecas"))

#Create new variable region into a factor and assign name of region as label
ls$climate <-factor(ls$state.code,
                   levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                   labels = c("Templada", "Calida.Ext", "Calida.Ext", "Tropical",
                              "Calida.Ext", "Templada", "Tropical", "Calida.Ext",
                              "Templada","Calida.Ext", "Templada", "Tropical",
                              "Templada", "Templada", "Templada", "Templada",
                              "Templada", "Templada", "Calida.Ext", "Tropical", 
                              "Templada", "Templada", "Tropical", "Templada",
                              "Calida.Ext", "Calida.Ext", "Tropical", "Calida.Ext",
                              "Templada", "Templada", "Tropical", "Templada"))

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state

ls$urb.code <- substr(ls$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
ls$urban <- factor(ls$urb.code, 
                     levels = c("0", "1", "2", "3", "4", "5", "6", "7", 
                                "8", "9"),
                     labels = c("Urban", "Urban", "Urban", "Urban", "Urban", "Urban", 
                                "Rural","Urban", "Urban", "Urban"))

#Survey design construction to compute statistical error
svd.ls <- svydesign(id =~upm, strata = ~est_socio, data = ls, weight=~factor_viv)

summary(svd.ls)
 
#Table Test
   ses.table <-svytable(~est_socio+state, design=svd.ls) %>%
     as.data.frame()
   
   summary(ses.table)
  
```
  
#Using Weights

```{r socioeconomic, include = FALSE , eval = TRUE}

#With Weights
summarytools::descr(select(ls, c('est_socio')),
                    weights = ls$factor_viv, style = "rmarkdown")
#Without Weights
summarytools::descr(select(ls, c('est_socio')),
                     style = "rmarkdown")

#Frequency Table

weight <- summarytools::freq(ls$est_socio, weights = ls$factor_viv)

weight

#Estimating frequency of SES 1
mean <-svymean(~est_socio, svd.ls, na.rm=TRUE)
var <-svyvar(~est_socio, svd.ls, na.rm = TRUE)
quantiles <- svyquantile(~est_socio, svd.ls,c(.25, .5, .75), ci= TRUE, na.rm = TRUE)

```  

#Average SES
```{r SES Mexico, include = FALSE}

#Set theme
theme_set(theme_classic())

#1. Find average socioeconomic status for Mexico: 2.14
    mean.mex <- svymean(~est_socio, svd.ls, na.rm=TRUE)
```    
```{r}    
#Mexico average SES 
mean.mex
```

```{r SES By State, include=FALSE}
#2. Find average socioeconomic status by state
  #Weighted Mean by State
   ses.state <-svyby(~est_socio, by=~state,
                           svd.ls, svymean,
                           na.rm=TRUE)
    ses.state %>%
      group_by(est_socio) %>%
      arrange(desc(est_socio))
    
    


      #Ggplot Scatterplot
      se.state.graph <- ggplot(ses.state, aes(x = state, y = est_socio, color = est_socio)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Socioeconomic Status", color = "Socioeconomic Status") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
       
```

```{r}
  #Proportion Table of SES by State
round(prop.table(table(ls$est_socio, ls$state), margin = 2),2)

wtd.chi.sq(ls$est_socio, ls$state, weight= ls$factor_viv)

wtd

#Dot Plot
se.state.graph+ geom_label_repel(aes(label = ses.state$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')

```
```{r SES By Climate, include = FALSE}
#3. Find weighted average socioeconomic status by climate
  ses.clim <-svyby(~est_socio, by=~climate,
                           svd.ls, svymean,
                           na.rm=TRUE)


 
   #Scatterplot
      se.climate.graph <- ggplot(ses.clim, aes(x = climate, y = est_socio, fill=climate)) +
        geom_bar(stat="identity") +
        scale_fill_viridis_d(option = "viridis") +
        labs(x = "Climate", y = "Socioeconomic Status", fill="Climate") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
      #Proportion Table of SES by Climate
prop.table(table(ls$est_socio, ls$climate), margin =2)
```
```{r}
#Proportion Table of SES by State
round(prop.table(table(ls$est_socio, ls$climate), margin = 2),2)    

  print(se.climate.graph)
```

```{r SES Urban vs. Rural, include = FALSE}
#4. Find average socioeconomic status: urban vs. rural
  ses.urb <-svyby(~est_socio, by=~urban,
                           svd.ls, svymean,
                           na.rm=TRUE)



       #Scatterplot
      se.urb.graph <- ggplot(ses.urb, aes(x = urban, y = est_socio, fill=urban)) +
        geom_bar(stat="identity") +
        scale_fill_viridis_d(option = "inferno", begin = 0.6, end = 1) +
        labs(x = "Location Type", y = "Socioeconomic Status", fill="Urban") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
      
      #AOV Attempt
      summary(aov(est_socio ~ factor(urban)*factor(state), data = ls))
```
```{r}
#Proportion Table of SES by State
round(prop.table(table(ls$est_socio, ls$urban), margin = 2), 2)

print(se.urb.graph)
```

#SES Frequency
```{r SES Freq Mexico, include = FALSE}
#1. Determine frequency of socioeconomic status categories in Mexico
  ses.freq <- svytable(~est_socio, design = svd.ls) %>%
  as.data.frame()

   ses.plot <- ggplot(ses.freq, aes(x = est_socio, y = Freq, fill = est_socio)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "House Type", y = "Frequency", fill = "Socioeconomic Status Category") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
        print(ses.plot) 
        
        
        #boxplot with weighted factors      
       ses.boxplot <- wtd.boxplot(ls$est_socio, weights = ls$est_socio)
       ses.boxplot
```
```{r SES Freq By State, include = FALSE}
#2. Determine frequency of socioeconomic status categories by state
    ses.freq.state <-svytable(~state+est_socio, design = svd.ls) %>%
      as.data.frame()
      
    
    ses.freq.state.plot <- ggplot(ses.freq.state, aes(x = state, y = Freq, fill = est_socio)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "State", y = "Frequency", fill = "Socioeconomic Status Category") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
    #boxplot with weighted factors      
         ses.state.box <- wtd.boxplot(ls$est_socio, weights = ls$factor_viv, plot = TRUE)
         ses.state.box
    
```
```{r}
    ses.freq.state.plot
```       
        
```{r SES Freq By Climate, include = FALSE}
#3. Determine frequency of socioeconomic status categories by climate
 ses.freq.clim <-svytable(~climate+est_socio, design = svd.ls) %>%
      as.data.frame()
      
    ses.freq.clim.plot <- ggplot(ses.freq.clim, aes(x = climate, y = Freq, fill = est_socio)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Climate", y = "Frequency", fill = "Socioeconomic Status Category") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
    
     
```
```{r}
    print(ses.freq.clim.plot) 
```

```{r SES Freq Urban vs Rural, include = FALSE}
#4. Determine frequency of socioeconomic status categories: Urban vs. Rural
     ses.freq.urb <-svytable(~urban+est_socio, design = svd.ls) %>%
      as.data.frame()
      
    
    ses.freq.urb.plot <- ggplot(ses.freq.urb, aes(x = urban, y = Freq, fill = est_socio)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Location Type", y = "Frequency", fill = "Socioeconomic Status Category") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
```        
```{r}        
print(ses.freq.urb.plot) 
  
```

#Housing Type Frequency
```{r Housing Type Mexico, include = FALSE}

ls$tipo_viv[ls$tipo_viv == "&"] <- NA

#1. Determine frequency of household types in Mexico

    tipo.freq <- svytable(~tipo_viv, svd.ls) %>%
  as.data.frame()

   tipo.plot <- ggplot(tipo.freq, aes(x = tipo_viv, y = Freq, fill = tipo_viv)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "House Type", y = "Frequency", fill = "House Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
```  
```{r}
print(tipo.plot) 
```

```{r Housing Type By State, include = FALSE}
#2. Determine frequency of household types by state

 tipo.freq.state <-svytable(~state+tipo_viv, design = svd.ls) %>%
      as.data.frame()
      
    tipo.freq.state.plot <- ggplot(tipo.freq.state, aes(x = state, y = Freq, fill = tipo_viv)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "State", y = "Frequency", fill = "Housing Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = c("Casa Independiente", "Departamento", "Vecinidad", "Azolea", "No Construida"))
  
```
```{r}
  
     #Proportion Table of SES  
        round(prop.table(table(ls$tipo_viv, ls$state, exclude = "&"), margin = 2),2)
 

   print(tipo.freq.state.plot) 
```

```{r Housing Type By Climate, include = FALSE}
#3. Determine frequency of household types by climate
tipo.freq.clim <-svytable(~climate+tipo_viv, design = svd.ls) %>%
      as.data.frame()
      
    tipo.freq.clim.plot <- ggplot(tipo.freq.clim, aes(x = climate, y = Freq, fill = tipo_viv)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Climate", y = "Frequency", fill = "Housing Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = c("Casa Independiente", "Departamento", "Vecinidad", "Azolea", "No Construida"))
    
    
     
    
```    
```{r}        
 #Proportion Table of SES  
        round(prop.table(table(ls$tipo_viv, ls$climate, exclude = "&"), margin = 2),2)   

 print(tipo.freq.clim.plot) 
```
```{r Housing Type Urban vs Rural, include = FALSE}
#4. Determine frequency of household types: Urban vs. Rural
tipo.freq.urb <-svytable(~urban+tipo_viv, design = svd.ls) %>%
      as.data.frame()
      
    tipo.freq.urb.plot <- ggplot(tipo.freq.urb, aes(x = urban, y = Freq, fill = tipo_viv)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Location Type", y = "Frequency", fill = "Housing Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) 


```
```{r}
  #Proportion Table of SES  
        round(prop.table(table(ls$tipo_viv, ls$urban, exclude = "&"), margin = 2),2)

    print(tipo.freq.urb.plot) 
      
```

#House Age
```{r Age of House Mexico, include = FALSE}
#1. Determine average house age in Mexico
 ha.mean.mex <- svymean(~antiguedad, svd.ls, na.rm=TRUE)
   ha.mean.mex

```
```{r Age of House By State, include = FALSE}
#2. Determine average house age by State
age.state <-svyby(~antiguedad, by=~state,
                           svd.ls, svymean,
                           na.rm=TRUE)
    age.state %>%
      arrange(desc(antiguedad))
    
      #Ggplot Scatterplot
      age.graph <- ggplot(age.state, aes(x = state, y = antiguedad, color = antiguedad)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Age of House", color = "Age of House") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      

```
```{r}
    #Linear Model  
   age.cor <- lm(ls$antiguedad ~ ls$state, data = ls, weights = factor_viv)
   summary(age.cor)
   
   #Checking Residuals
   qqnorm(age.cor$residuals)
   
#Scatterplot
age.graph + geom_label_repel(aes(label = age.state$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')
```

```{r Age of House By Climate, include = FALSE}
#3. Determine average house age by climate
age.clim <-svyby(~antiguedad, by=~climate,
                           svd.ls, svymean,
                           na.rm=TRUE)
    age.clim %>%
      arrange(desc(antiguedad))

      #Ggplot Scatterplot
      age.clim.graph <- ggplot(age.clim, aes(x = climate, y = antiguedad, fill = antiguedad)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "magma", direction = -1) +
        labs(x = "Climate", y = "Age of House", color = "Age of House") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
 #Linear Model  
   age.cor <- lm(ls$antiguedad ~ ls$climate, data = ls, weights = factor_viv)
   summary(age.cor)
   
   #Checking Residuals
   qqnorm(age.cor$residuals)

print(age.clim.graph)
```

```{r Urban vs Rural, include = FALSE}
age.urb <-svyby(~antiguedad, by=~urban,
                           svd.ls, svymean,
                           na.rm=TRUE)
    age.urb %>%
      arrange(desc(antiguedad))

      #Ggplot Scatterplot
      age.urb.graph <- ggplot(age.urb, aes(x = urban, y = antiguedad, fill = antiguedad)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma", direction = -1) +
        labs(x = "Area Type", y = "Age of House", color = "Age of House") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
 age.cor <- lm(ls$antiguedad ~ ls$urban, data = ls, weights = factor_viv)
   summary(age.cor)
   
   #Checking Residuals
   qqnorm(age.cor$residuals)       

print(age.urb.graph)
```
# Average House Size
```{r House Size Mexico, include = FALSE}
#1. Determine average house size in Mexico (# rooms)
 hs.mean.mex <- svymean(~num_cuarto, svd.ls, na.rm=TRUE)
    hs.mean.mex
```
```{r House Size By State, include = FALSE}
#2. Determine average house size by state
 cuar.state <-svyby(~num_cuarto, by=~state,
                           svd.ls, svymean,
                           na.rm=TRUE)
    cuar.state %>%
      arrange(desc(num_cuarto))

      #Ggplot Scatterplot
      cuar.graph <- ggplot(cuar.state, aes(x = state, y = num_cuarto, color = num_cuarto)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Number of Rooms in Home", color = "Number of Rooms") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
         cuar.graph + geom_label_repel(aes(label = cuar.state$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')
 
```
```{r House Size By Climate, include = FALSE}
#3. Determine average house size by climate
 cuar.clim <-svyby(~num_cuarto, by=~climate,
                           svd.ls, svymean,
                           na.rm=TRUE)
 
   #Scatterplot
      cuar.clim.graph <- ggplot(cuar.clim, aes(x = climate, y = num_cuarto, fill=climate)) +
        geom_bar(stat="identity") +
        scale_fill_viridis_d(option = "viridis") +
        labs(x = "Climate", y = "Number of Rooms in House", fill="Climate") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
 size.cor <- lm(ls$num_cuarto ~ ls$climate, data = ls, weights = factor_viv)
   summary(size.cor)
   
   #Checking Residuals
   qqnorm(size.cor$residuals)          

   print(cuar.clim.graph)
```
```{r House Size Urban vs. Rural, include = FALSE}
#3. Determine average house size by climate
 cuar.urb <-svyby(~num_cuarto, by=~urban,
                           svd.ls, svymean,
                           na.rm=TRUE)
 
   #Scatterplot
      cuar.urb.graph <- ggplot(cuar.urb, aes(x = urban, y = num_cuarto, fill=urban)) +
        geom_bar(stat="identity") +
        scale_fill_viridis_d(option = "viridis") +
        labs(x = "Location Type", y = "Number of Rooms in House", fill="Urban") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
 size.cor <- lm(ls$num_cuarto ~ ls$urban, data = ls, weights = factor_viv)
   summary(size.cor)
   
   #Checking Residuals
   qqnorm(size.cor$residuals)          


print(cuar.urb.graph)

```

#House Size Frequency

```{r House Size Freq All Mexico, include = FALSE}
#1. Determine Frequency of House Sizes in Mexico 
cuar.freq <- svytable(~num_cuarto, design = svd.ls) %>%
  as.data.frame()

   cuar.plot <- ggplot(cuar.freq, aes(x = num_cuarto, y = Freq, fill = num_cuarto)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "House Size", y = "Frequency", fill = "Number of Rooms") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
```
```{r}
    print(cuar.plot) 
      
```      
```{r House Size Freq By State, include = FALSE}
#2. Determine Frequency of House Sizes in Mexico by State
 cuar.freq.state <-svytable(~state+num_cuarto, design = svd.ls) %>%
      as.data.frame()
      
    
    cuar.freq.state.plot <- ggplot(cuar.freq.state, aes(x = state, y = Freq, fill = num_cuarto)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "State", y = "Frequency", fill = "Number of Rooms in Home") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
      
    
```
```{r}
#Proportion Table  
        round(prop.table(table(ls$num_cuarto, ls$state, exclude = c("11", "12", "13", "14", "15", "17")), margin = 2),2)  

  print(cuar.freq.state.plot) 
```
```{r House Size Freq By Climate, include = FALSE}
#3. Determine Frequency of House Sizes in Mexico by Climate
 cuar.freq.clim <-svytable(~climate+num_cuarto, design = svd.ls) %>%
      as.data.frame()
      
    
    cuar.freq.clim.plot <- ggplot(cuar.freq.clim, aes(x = climate, y = Freq, fill = num_cuarto)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Climate", y = "Frequency", fill = "Number of Rooms in Home") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
    
    
```        
```{r}
 round(prop.table(table(ls$num_cuarto, ls$climate, exclude = c("11", "12", "13", "14", "15", "17")), margin = 2),2)   

 print(cuar.freq.clim.plot) 
         
```
```{r House Size Freq Urban vs. Rural, include = FALSE}
#4. Determine Frequency of House Sizes in Mexico: Urban vs Rural
 cuar.freq.urb <-svytable(~urban+num_cuarto, design = svd.ls) %>%
      as.data.frame()
      
    
    cuar.freq.urb.plot <- ggplot(cuar.freq.urb, aes(x = urban, y = Freq, fill = num_cuarto)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Location Type", y = "Frequency", fill = "Number of Rooms in Home") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
         
    

    
```
```{r}
#Proportion Table
    round(prop.table(table(ls$num_cuarto, ls$urban, exclude = c("11", "12", "13", "14", "15", "17")), margin = 2),2)  

  print(cuar.freq.urb.plot) 
         
```

#Urban vs Rural

```{r Urban v Rural Mexico, include = FALSE}
#1. Determine Rural vs. Urban in Mexico 

urb.freq <- svytable(~urban, design = svd.ls) %>%
  as.data.frame()

   urb.plot <- ggplot(urb.freq, aes(x = urban, y = Freq, fill = urban)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "plasma", direction = -1) + 
          labs(x = "Area Type", y = "Frequency", fill = "Area Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
        
```
```{r}
   print(urb.plot) 
     
```
```{r Urban v Rural By State, include = FALSE}
       
#2.Rural Vs. Urban in Mexico by State
urb.freq.state <-svytable(~state+urban, design = svd.ls) %>%
      as.data.frame()
      
    
    urb.freq.state.plot <- ggplot(urb.freq.state, aes(x = state, y = Freq, fill = urban)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma") + 
          labs(x = "Area Type", y = "Frequency", fill = "Area Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
   

```
```{r}
 #Proportion Table
         round(prop.table(table(ls$urban, ls$state), margin = 2),2)    

print(urb.freq.state.plot) 
 
```

```{r Urban v Rural By Climate, include = FALSE}
#3. Urban vs. Rural in Mexico by Climate
urb.freq.climate <-svytable(~climate+urban, design = svd.ls) %>%
      as.data.frame()
      
    
    urb.freq.climate.plot <- ggplot(urb.freq.climate, aes(x = climate, y = Freq, fill = urban)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Area Type", y = "Frequency", fill = "Area Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
    
    
    
```
```{r}
#2-Way Table
    round(prop.table(table(ls$urban, ls$climate), margin = 2),2)  

  print(urb.freq.climate.plot) 
     
```

#Tenancy
```{r Tenancy Mexico, include = FALSE}
#1. Tenancy Frequency Across Mexico 
       
ten.freq <- svytable(~tenencia, design = svd.ls) %>%
  as.data.frame()

   ten.plot <- ggplot(ten.freq, aes(x = tenencia, y = Freq, fill = tenencia)) +
          geom_bar(stat="identity", position = position_dodge()) +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Tenancy Type", y = "Frequency", fill = "Tenancy Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
```
```{r}
      

     print(ten.plot) 
       
```      
```{r Tenancy By State, include = FALSE}       
#2.Tenancy in Mexico by State
ten.freq.state <-svytable(~state+tenencia, design = svd.ls) %>%
      as.data.frame()
      
    
    ten.freq.state.plot <- ggplot(ten.freq.state, aes(x = state, y = Freq, fill = tenencia)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Tenancy Type", y = "Frequency", fill = "Tenancy Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
    
    
```
```{r}
 #2-Way Table
    round(prop.table(table(ls$tenencia, ls$state), margin = 2),2)  

  print(ten.freq.state.plot) 
```
```{r By Climate, include = FALSE}
#3. Determine Tenancy in Mexico by Climate
 ten.freq.climate <-svytable(~climate+tenencia, design = svd.ls) %>%
      as.data.frame()
      
    #Bar plot
    ten.freq.climate.plot <- ggplot(ten.freq.climate, aes(x = climate, y = Freq, fill = tenencia)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Tenancy Type", y = "Frequency", fill = "Tenancy Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
   
    
```
```{r}
 #2-Way Table
    round(prop.table(table(ls$tenencia, ls$climate), margin = 2),2)   

 print(ten.freq.climate.plot) 
```
```{r Tenancy Urban vs. Rural, include = FALSE}
#4. Determine Tenancy in Mexico: Urban vs. Rural
ten.freq.urb <-svytable(~urban+tenencia, design = svd.ls) %>%
      as.data.frame()
      
    
    ten.freq.urb.plot <- ggplot(ten.freq.urb, aes(x = urban, y = Freq, fill = tenencia)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "magma") + 
          labs(x = "Tenancy Type", y = "Frequency", fill = "Tenancy Type") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
   
```
```{r}
 #2-Way Table
    round(prop.table(table(ls$tenencia, ls$urban), margin = 2),2)  

  print(ten.freq.urb.plot)     
```

#Household Income
```{r Income Mexico, include = FALSE}
#1. Average Houehold Income: Mexico 
 in.mean.mex <- svymean(~ing_cor, svd.ls, na.rm=TRUE)
    in.mean.mex
```
```{r Income By State, include = FALSE}
#2.Average Houehold Income by State
in.state <-svyby(~ing_cor, by=~state,
                           svd.ls, svymean,
                           na.rm=TRUE)
    in.tot <- in.state %>%
      mutate(`total` = in.state$ing_cor*4)
    
      in.tot.usd <- in.tot %>%
      mutate(`usd` = in.tot$total*.052)
      
      #Ggplot Scatterplot Peso
      in.graph <- ggplot(in.tot, aes(x = state, y = total, color = total)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Average Income (Peso/Yr)", color = "Average Income (Peso/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
        #Ggplot Scatterplot USD
      in.graph.usd <- ggplot(in.tot.usd, aes(x = state, y = usd, color = usd)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Average Income (USD/Yr)", color = "Average Income (USD/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
       
    
   
  
      
```
```{r}
  #Linear Model Age of House by Income
      age.inc.cor <- lm(ls$antiguedad ~ ls$ing_cor, data = ls, weights = factor_viv)
   summary(age.inc.cor)
   #Checking Residuals
   qqnorm(age.inc.cor$residuals)
   
   #Linear Model Energy Cost vs Annual Income
   in.ene.cor <- lm((ls$energia) ~ ls$ing_cor, data = ls, weights = factor_viv)
   summary(in.ene.cor)
   
   #Checking Residuals
   qqnorm(in.ene.cor$residuals)    


   in.graph+ geom_label_repel(aes(label = in.tot$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')

in.graph.usd+ geom_label_repel(aes(label = in.tot.usd$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')
```
```{r Income By Climate}
#3. Determine Mean Income in Mexico by Climate
in.climate <-svyby(~ing_cor, by=~climate,
                           svd.ls, svymean,
                           na.rm=TRUE)

     in.tot.clim <- in.climate %>%
      mutate(`total` = in.climate$ing_cor*4)
    
      in.tot.clim.usd <- in.tot.clim %>%
      mutate(`usd` = in.tot.clim$total*.052)
      
      #Ggplot Scatterplot
      in.clim.graph <- ggplot(in.tot.clim, aes(x = climate, y = total, fill = total)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Climate", y = "Average Income (Peso/Yr)", fill = "Average Income(Peso/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
          #Ggplot Scatterplot USD
      in.clim.usd.graph <- ggplot(in.tot.clim.usd, aes(x = climate, y = usd, fill = usd)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Climate", y = "Average Income (USD/Yr)", fill = "Average Income(USD/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
        print(in.clim.graph)
        print(in.clim.usd.graph)

```

```{r Income Urban vs Rural, include = FALSE}
#4. Determine Average Income in Mexico: Rural vs Urban
  in.urb <-svyby(~ing_cor, by=~urban,
                           svd.ls, svymean,
                           na.rm=TRUE)
   in.tot.urb <- in.urb %>%
      mutate(`total` = in.urb$ing_cor*4)
    
      in.tot.urb.usd <- in.tot.urb %>%
      mutate(`usd` = in.tot.urb$total*.052)

      #Ggplot Scatterplot
      in.urb.graph <- ggplot(in.tot.urb, aes(x = urban, y = total, fill = total)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Area Type", y = "Average Income (Peso/Yr)", fill = "Average Income (Peso/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
       #Ggplot Scatterplot
      in.urb.usd.graph <- ggplot(in.tot.urb.usd, aes(x = urban, y = usd, fill = usd)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Area Type", y = "Average Income (USD/Yr)", fill = "Average Income (USD/Yr)") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}
      print(in.urb.graph)
      print(in.urb.usd.graph)
```

#Energy Expenditure
```{r Energy Mexico, include = FALSE}

#1. Average Energy Expendiure: Mexico 
 ene.mean.mex <- svymean(~energia, svd.ls, na.rm=TRUE)
    ene.mean.mex
```

```{r Energy By State, include = FALSE}
#2.Average Energy Expenditure by State
ene.state <-svyby(~energia, by=~state,
                           svd.ls, svymean,
                           na.rm=TRUE)
    ene.state %>%
      arrange(desc(energia))

      #Ggplot Scatterplot
      ene.graph <- ggplot(ene.state, aes(x = state, y = energia, color = energia)) +
        geom_point() +
        scale_color_viridis_c(option = "inferno", direction = -1) +
        labs(x = "State", y = "Average Energy Expenditure", color = "Average Energy Expenditure") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
      
      
      
```
```{r}
       ene.graph + geom_label_repel(aes(label = ene.state$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')
```     

```{r Energy  By Climate, include = FALSE}
#3. Determine Tenancy in Mexico by Climate
  ene.climate <-svyby(~energia, by=~climate,
                           svd.ls, svymean,
                           na.rm=TRUE)
    ene.climate %>%
      arrange(desc(energia))

      #Ggplot Barplot
      ene.clim.graph <- ggplot(ene.climate, aes(x = climate, y = energia, fill = energia)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Climate", y = "Average Energy Expenditure", fill = "Average Energy Ependiture") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
```{r}

clim.ene.cor <- lm(energia ~ climate, data = ls, weights = factor_viv)   
summary(clim.ene.cor)
qqnorm(clim.ene.cor$residuals)    

print(ene.clim.graph)
```

```{r Energy Urban vs Rural, include = FALSE}
#4. Determine Energy in Expenditure in Mexico by Urban vs. Rural
      
 ene.urb <-svyby(~energia, by=~urban,
                           svd.ls, svymean,
                           na.rm=TRUE)

  #Transform to Yearly
    ene.urb.yr <- ene.urb %>%
      mutate(`yearly` = ene.urb$energia*4)%>%
      arrange(desc(yearly))
    
    #Transform to USD
    ene.urb.yr.usd <- ene.urb.yr %>%
      mutate(`usd` = ene.urb.yr$yearly*.052)%>%
      arrange(desc(usd))


      #Ggplot Scatterplot
      ene.urb.graph <- ggplot(ene.urb.yr, aes(x = urban, y = yearly, fill = yearly)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Area Type", y = "Average Energy Expenditure (Peso/Yr)", fill = "Average Energy Expenditure (Peso/Yr)")+
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
      
      
      #Ggplot Scatterplot
      ene.urb.graph.usd <- ggplot(ene.urb.yr.usd, aes(x = urban, y = usd, fill = usd)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Area Type", y = "Average Energy Expenditure (USD/Yr)", fill = "Average Energy Expenditure (USD/Yr)")+
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
```

```{r}
urb.ene.cor <- lm(energia ~ urban, data = ls, weights = factor_viv)    
summary(urb.ene.cor)
qqnorm(urb.ene.cor$residuals)  

print(ene.urb.graph)
      print(ene.urb.graph.usd)
```

#Energy Burden
```{r Energy Burden Mexico, include = FALSE}

#1. Average Energy Expendiure: Mexico 
 ene.mean.mex <- svymean(~energia, svd.ls, na.rm=TRUE)
    100*(round(ene.mean.mex/ in.mean.mex,4))
    
    
     #Linear Model Age of House and Energy Cost
   age.ene.cor <- lm(ls$energia ~ ls$antiguedad, data = ls)
   summary(age.ene.cor)
    
```    
```{r Energy Burden By State, include = FALSE}

#2. Energy burden: State

       ene.mean.mex <- svymean(~energia, svd.ls, na.rm=TRUE)
    burden <- 100*(round(ene.state$energia/in.state$ing_cor, 4))
    print(burden)
    
    #create new data frame with energy burden
    burden.state <- as.data.frame(x = ene.state$state)
    burden.state$burden <- burden
   
     print(burden.state)
    
     #Ggplot Scatterplot
      burden.state.graph <- ggplot(burden.state, aes(x = ene.state$state, y = burden, color = burden)) +
        geom_point() +
        scale_color_viridis_c(option = "viridis", direction = -1) +
        labs(x = "State", y = "Average Energy Burden", color = "Average Energy Burden") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
      
      
```
```{r}
        burden.state.graph + geom_label_repel(aes(label = ene.state$state), box.padding = 0.35, point.padding = 0.5, segment.color = 'grey50')
   
```    
```{r Energy Burden By Climate, include = FALSE}

#3. Energy burden: Climate
 ene.mean.mex <- svymean(~energia, svd.ls, na.rm=TRUE)
    burden <- 100*(round(ene.climate$energia/in.climate$ing_cor, 4))
    print(burden)
    
    #create new data frame with energy burden
    burden.climate <- as.data.frame(x = ene.climate$climate)
    burden.climate$burden <- burden
    print(burden.climate)
    
      #Ggplot Barplot
      burden.climate.graph <- ggplot(burden.climate, aes(x = ene.climate$climate, y = burden, fill = burden)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Climate", y = "Average Energy Burden",  fill = "Average Energy Burden") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))+
      geom_text(aes(label = ene.climate$climate), vjust = -0.5)
      
      
       #Linear Model Age of House and Energy Cost
   #burd.clim.cor <- lm(burden.climate$burden ~ ls$climate, data = ls)
   #summary(burd.clim.cor)
```      
```{r}
          
  print(burden.climate.graph)
   
```    
```{r Energy Burden Urban vs Rural, include = FALSE}

#3. Energy burden: Urban vs Rural
 ene.mean.mex <- svymean(~energia, svd.ls, na.rm=TRUE)
    burden <- 100*(round(ene.urb$energia/in.urb$ing_cor, 4))
    print(burden)
    
    #create new data frame with energy burden
    burden.urb <- as.data.frame(x = ene.urb$urb)
    burden.urb$burden <- burden
    print(burden.urb)
    
      #Ggplot Barplot
      burden.urb.graph <- ggplot(burden.urb, aes(x = ene.urb$urb, y = burden, fill = burden)) +
        geom_bar(stat = "identity") +
        scale_fill_viridis_c(option = "plasma") +
        labs(x = "Urban vs Rural", y = "Average Energy Burden", color = "Average Energy Burden", fill = "Average Energy Burden") + 
      geom_text(aes(label = ene.urb$urb), vjust = -.5)
      
```
```{r}
  print(burden.urb.graph)
  
```
