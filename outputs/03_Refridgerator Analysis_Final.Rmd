---
title: "Master num_refriwaves"
author: "Jess Siegel"
date: "12/15/2019"
output: html_document
---

```{r setup, include=FALSE}
rm(list=ls())

#Set WD
setwd("~/Desktop/MP/mx_electricity_residential/outputs")
#Load Packages
if (!require("pacman")) install.packages("pacman")
  pacman::p_load(pacman, party, psych, rio, BiocManager, tidyverse, ggplot2, magrittr, plyr, knitr, dplyr, ggplot2, survey, summarytools, ENmisc, dplyr, xtable, formattable, reshape, reshape2, tables)

opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
```

```{r upload, include= FALSE}
f18<-read_csv("../inputs/df18.csv")
f16<-read_csv("../inputs/df16.csv")
f14<-read_csv("../inputs/df14.csv")
f12<-read_csv("../inputs/df12.csv")
f10<-read_csv("../inputs/df10.csv")
f08<-read_csv("../inputs/df08.csv")

f18<-f18 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad","num_refri", "anio_refri", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f16<-f16 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad","num_refri", "anio_refri", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f14<-f14 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad","num_refri", "anio_refri", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f12<-f12 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad","num_refri", "anio_refri", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

f10<-f10 %>%
  select("folioviv", "upm", "factor", "est_socio", "antiguedad","num_refri", "anio_refri", "state","climate", "tipo_viv", "energia_USD", "ing_cor_USD", "year", "urban", "tam_loc", "est_dis", "houseage", "num_room")  %>% mutate(tipo_viv = as.factor(tipo_viv))%>% mutate(est_socio=as.factor(est_socio))%>% mutate(tam_loc=as.factor(tam_loc))%>% mutate(climate=as.factor(climate))%>% mutate(urban=as.factor(urban))%>% mutate(state=as.factor(state))%>% mutate(house_age=as.factor(houseage))%>% mutate(num_room=as.factor(num_room))

fnew<-rbind(f08, f10, f12, f14, f16, f18)


fnew$refri<-fnew$num_refri
fnew$refri[fnew$refri>0]=1

f18$refri<-f18$num_refri
f18$refri[f18$refri>0]=1

f16$refri<-f16$num_refri
f16$refri[f16$refri>0]=1

f14$refri<-f14$num_refri
f14$refri[f14$refri>0]=1

f12$refri<-f12$num_refri
f12$refri[f12$refri>0]=1

f10$refri<-f10$num_refri
f10$refri[f10$refri>0]=1

f08$refri<-f08$num_refri
f08$refri[f08$refri>0]=1

```


#Summary of relevant data
```{r survey_design, include=FALSE, echo=TRUE}

summary_1<-summarytools::descr( fnew, weights = fnew$factor, strata=~fnew$est_dis, style = "rmarkdown")
summary_2018<-summarytools::descr( f18, weights = f18$factor, strata=~f18$est_dis, style = "rmarkdown")
summary_2016<-summarytools::descr( f16, weights = f16$factor, strata=~f16$est_dis, style = "rmarkdown")
summary_2014<-summarytools::descr( f14, weights = f14$factor, strata=~f14$est_dis, style = "rmarkdown")
summary_2012<-summarytools::descr( f12, weights = f12$factor, strata=~f12$est_dis, style = "rmarkdown")
summary_2010<-summarytools::descr( f10, weights = f10$factor, strata=~f10$est_dis, style = "rmarkdown")
summary_2008<-summarytools::descr( f08, weights = f08$factor, strata=~f08$est_dis, style = "rmarkdown")


summary_2018
summary_2016
summary_2014
summary_2012
summary_2010
summary_2008

svd.fnew <- svydesign(id =~upm, data = fnew, weight=~factor, strata=fnew$est_dis, nest=TRUE)

```

#Adoption of Energy Saving Lightbubls in Mexico

##proportion of bulbs that are energy saving in Mexico
```{r prop_refri_ALL, include=TRUE, echo =FALSE}

mean.num_refri.year <- svyby(~num_refri, 
                           by=~year, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)
ggplot(data=mean.num_refri.year, 
       aes(x=year, y=num_refri, 
           label = rownames(num_refri) ))+
  geom_bar(stat="identity", fill= "steelblue",  position = "dodge", width=0.5) + 
  geom_errorbar(aes(ymin=num_refri-se, ymax=num_refri+se),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
  geom_text(aes(label=round(num_refri,3)), vjust=3.5, color="white", size=3.5) +
  scale_x_discrete(name= "Year", limits=c(2010, 2012, 2014, 2016, 2018))+
  ggtitle("Average # of num_refriwaves per households") +
  theme_bw()
```

##Proportion of households with num_refriwaves
```{r num_refri_ALL, include=TRUE, echo =FALSE}

mean.num_refri.year <- svyby(~num_refri, by=~year, svd.fnew, svymean, na.rm=FALSE)

mean.num_refri.year<-mean.num_refri.year %>%as.data.frame()%>%rename(c('num_refri'='adopt.rate'))


freq<- svytable(~num_refri+year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$num_refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$num_refri == 0, ] %>%rename(c('Freq'='Freq2'))
freq2<-merge(freq2, freq3, by=c("year"))
freq2$total.households=freq2$Freq2+freq2$Freq

#merge:
mean.num_refri.year<-merge(mean.num_refri.year, freq2, by=c("year")) %>% select("year", "adopt.rate", "se", "total.households")


write.csv(mean.num_refri.year,"~/Desktop/MP/mx_electricity_residential/outputs/refri/mean.num_refri.year.csv", row.names = FALSE)
```

```{r refri_num_socio, include=TRUE}

#proportion of bulbs that are energy saving
mean.total<- svyby(~num_refri, by=~year+est_socio, svd.fnew, svymean, na.rm=TRUE)

mean.prop.year <- svyby(~num_refri, 
                           by=~year+est_socio, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

#Plots proportion of bulbs that are energy savings for all 3 years that have adopted energy saving bulbs by socioeconomic status.

q<-ggplot(mean.total,aes(x=year,y=num_refri, fill=factor(est_socio)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Socioeconomic",
                      breaks=c(1, 2, 3, 4),
                      labels=c("low","low-medium", "high-medium","high"))+ 
  scale_x_discrete(name= "Year", limits=c(2010, 2012, 2014, 2016, 2018)) +
  geom_errorbar(position=position_dodge(width=.9), width=.5) + aes(ymin=num_refri-se, ymax=num_refri+se)+ 
  geom_text(aes(label=round(num_refri, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Proportion of Energy Saving Bulbs in Mexico by Socioeconomic Status", caption ="Number in percent", y="Proportion of Bulbs")
```

The charts below summarize the frequency and proportion of households that have a num_refriwave, by year & socioeconomic status. These numbers are followed by a graphical representation of the data.

```{r num_refri_socio, include=TRUE, echo = FALSE}
#proportion of households with refridgerators
mean.total<- svyby(~refri, by=~year+est_socio, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~refri+year+est_socio, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$refri == 1, ]
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio"))
freq2$total.households=freq2$Freq2+freq2$Freq

socio_refri<- merge(freq2, mean.total2, by=c("year", "est_socio"))%>%select("est_socio", "year", "refri", "se", "total.households")%>%rename(c('refri'='adopt.rate'))

q<-ggplot(mean.total,aes(x=year,y=refri, fill=factor(est_socio)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Socioeconomic",
                      breaks=c(1, 2, 3, 4),
                      labels=c("low","low-medium", "high-medium","high"))+ 
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018)) +
  geom_errorbar(position=position_dodge(width=.9), width=.5) + aes(ymin=refri-se, ymax=refri+se)+ 
  geom_text(aes(label=round(refri, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households Adoption of Energy Saving Bulbs in Mexico by Socioeconomic Status", caption ="Number in percent", y="Proportion of Households")

write.csv(socio_refri,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_refri", row.names = FALSE)
```

```{r refri_urban, include=TRUE}

#proportion of households with energy saving bulbs
mean.total<- svyby(~refri, by=~year+urban, svd.fnew, svymean, na.rm=TRUE)
mean.total2<-mean.total %>%as.data.frame()

freq<- svytable(~refri+year+urban, design = svd.fnew) %>%as.data.frame()

#select only households with energy saving bulbs
freq2<-freq[freq$refri == 1, ]
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","urban"))
freq2$total.households=freq2$Freq2+freq2$Freq

urban_refri<- merge(freq2, mean.total2, by=c("year", "urban"))%>%select("urban", "year", "refri", "se", "total.households")%>%rename(c('refri'='adopt.rate'))

q<-ggplot(mean.total,aes(x=year,y=refri, fill=factor(urban)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Urban")+ 
  scale_x_discrete(name= "Year", limits=c(2010, 2012, 2014, 2016, 2018)) +
  geom_text(aes(label=round(refri, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households Adoption of Energy Saving Bulbs in Urban v. Rural Households", caption ="Number in percent", y="Proportion of Households")

write.csv(urban_refri,"~/Desktop/MP/mx_electricity_residential/outputs/refri/urban_refri.csv", row.names = FALSE)

```

```{r refribulbs_tipo_viv, include=TRUE}
#proportion of households with refriwaves
mean.total <- svyby(~refri, 
                           by=~year+tipo_viv, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

mean.total2<-mean.total %>%as.data.frame()
freq<- svytable(~refri+year+tipo_viv, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$refri == 1, ]
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","tipo_viv"))
freq2$total.households=freq2$Freq2+freq2$Freq

tipo_viv_refri<- merge(freq2, mean.total2, by=c("year", "tipo_viv"))%>%select("tipo_viv", "year", "refri", "se", "total.households")%>%rename(c('refri'='adopt.rate'))

tipo_viv_refri

q<-ggplot(mean.total,aes(x=year,y=refri, fill=factor(tipo_viv)))+ geom_bar(stat="identity",position="dodge")+
  scale_fill_discrete(name="Housing Type",
                      breaks=c("house", "apartment", "other"),
                      labels=c("Houses","Apartments", "Other"))+ 
  scale_x_discrete(name= "Year", limits=c(2014, 2016, 2018)) +
  geom_text(aes(label=round(refri, 3)), vjust=2.6, color="white", 
            position = position_dodge(.9), size=2)


q + theme(axis.text.x = element_text(angle = 90, hjust = 1))  +  labs(title = "Households Adoption of Energy Saving Bulbs in Mexico by Housing Type", caption ="Number in percent", y="Proportion of Households")

write.csv(tipo_viv_refri,"~/Desktop/MP/mx_electricity_residential/outputs/refri/tipo_viv_refri.csv", row.names = FALSE)
```


```{r refribulbs_houseage, include=TRUE}
#proportion of households with energy saving bulbs
mean.total <- svyby(~refri, 
                           by=~year+houseage, 
                           svd.fnew, svymean, 
                           na.rm=TRUE)

mean.total2<-mean.total %>%as.data.frame()
freq<- svytable(~refri+year+houseage, design = svd.fnew) %>%as.data.frame()
freq2<-freq[freq$refri == 1, ]
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","houseage"))
freq2$total.households=freq2$Freq2+freq2$Freq

houseage_refri<- merge(freq2, mean.total2, by=c("year", "houseage"))%>%select("houseage", "year", "refri", "se", "total.households")%>%rename(c('refri'='adopt.rate'))

houseage_refri

write.csv(houseage_refri,"~/Desktop/MP/mx_electricity_residential/outputs/refri/houseage_refri.csv", row.names = FALSE)
```

##Look at housheolds with energy saving bulbs by year, locality size, and income level
```{r socio_tam, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~year+tam_loc+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + year + est_socio +tam_loc, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "tam_loc"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_tam<- merge(freq2, mean.total2, by=c("year", "est_socio", "tam_loc")) %>% select("year", "est_socio", "tam_loc", "adopt.rate", "se", "total.households")

write.csv(socio_tam,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_tam.csv", row.names = FALSE)
```

The charts below shows the household adoption rate of lightbulbs given socioeconomic status and housing type in each year:

```{r socio_tipo, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~year+tipo_viv+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + year+ est_socio +tipo_viv, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "tipo_viv"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_tipo<- merge(freq2, mean.total2, by=c("year","est_socio", "tipo_viv")) %>% select("year", "est_socio", "tipo_viv", "adopt.rate", "se", "total.households")
#%>%rename(c('est_socio'='Socioeconomic','tipo_viv'='Locality_Size'))

socio_tipo

write.csv(socio_tipo,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_tipo.csv", row.names = FALSE)
```

The chart below shows the household adoption rate of lightbulbs given socioeconomic status and urban/rural status

```{r socio_urb, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~year+urban+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + year + est_socio +urban, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "urban"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_urb<- merge(freq2, mean.total2, by=c("year", "est_socio", "urban")) %>% select("year", "est_socio", "urban", "adopt.rate", "se", "total.households")

write.csv(socio_urb,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_urb.csv", row.names = FALSE)
```

The chart below shows the household adoption rate of lightbulbs given socioeconomic status and climate status for each year 
```{r socio_clima, include=TRUE, echo=FALSE}

mean.total<- svyby(~refri, by=~year+climate+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)

mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + year+est_socio +climate, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "climate"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_clima<- merge(freq2, mean.total2, by=c("year", "est_socio", "climate")) %>% select("year", "est_socio", "climate", "adopt.rate", "se", "total.households")

write.csv(socio_clima,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_clima.csv", row.names = FALSE)
```

The following charts show the adoption rate of energy saving lightbulbs given the size of the house and the socioeconomic status of the home

```{r socio_urb_clima, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~year+num_room+est_socio, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + year+est_socio +num_room, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "num_room"))
freq2$total.households=freq2$Freq2+freq2$Freq


#Merge into one table:
socio_size<- merge(freq2, mean.total2, by=c("year","est_socio", "num_room")) %>% select("year", "est_socio", "num_room", "adopt.rate", "se", "total.households")

socio_size #dataframe/with all data

write.csv(socio_size,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_size.csv", row.names = FALSE)
```

#4-way Table
##All Years
```{r socio_urban_climate18, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~est_socio + urban + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + est_socio +urban +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "urban", "climate"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_urban_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "urban", "climate")) %>% select("year", "est_socio", "urban","climate", "adopt.rate", "se", "total.households")
#%>%rename(c('est_socio'='Socioeconomic','urban'='Locality_Size'))

write.csv(socio_urban_climate,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_urban_climate_all.csv", row.names = FALSE)
```


```{r socio_viv_climateall, include=TRUE, echo=FALSE}
mean.total<- svyby(~refri, by=~est_socio + tipo_viv + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + est_socio +tipo_viv + climate + year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "tipo_viv", "climate"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_tipo_viv_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "tipo_viv", "climate")) %>% select("year", "est_socio", "tipo_viv","climate", "adopt.rate", "se", "total.households")
#%>%rename(c('est_socio'='Socioeconomic','tipo_viv'='Locality_Size'))

socio_tipo_viv_climate$adopt.rate<-socio_tipo_viv_climate$adopt.rate

socio_tipo_viv_climate #dataframe/with all data

write.csv(socio_tipo_viv_climate,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_tipo_viv_climate_all.csv", row.names = FALSE)
```

```{r socio_viv_climateall, include=TRUE, echo=FALSE}

mean.total<- svyby(~refri, by=~est_socio + num_room + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adopt.rate'))

freq<- svytable(~refri + est_socio +num_room +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "num_room", "climate"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_num_room_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "num_room", "climate")) %>% select("year", "est_socio", "num_room","climate", "adopt.rate", "se", "total.households")
#%>%rename(c('est_socio'='Socioeconomic','num_room'='Locality_Size'))

socio_num_room_climate$adopt.rate<-socio_num_room_climate$adopt.rate

socio_num_room_climate #dataframe/with all data

write.csv(socio_num_room_climate,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_num_room_climate_all.csv", row.names = FALSE)
```


```{r socio_tam_loc_climateall, include=TRUE, echo=FALSE}

mean.total<- svyby(~refri, by=~est_socio + tam_loc + climate + year, svd.fnew, svymean, na.rm=TRUE, keep.var=TRUE)
mean.total2<-mean.total %>%as.data.frame()%>%rename(c('refri'='adoption'))

freq<- svytable(~refri + est_socio +tam_loc +climate +year, design = svd.fnew) %>% as.data.frame()
freq2<-freq[freq$refri == 1, ]%>%as.data.frame() #selects just those that have been adopted
freq3<-freq[freq$refri == 0, ]%>%rename(c("Freq"= "Freq2"))
freq2<-merge(freq2, freq3, by=c("year","est_socio", "tam_loc", "climate"))
freq2$total.households=freq2$Freq2+freq2$Freq

#Merge into one table:
socio_tam_loc_climate<- merge(freq2, mean.total2, by=c("year", "est_socio", "tam_loc", "climate")) %>% select("year", "est_socio", "tam_loc","climate", "adoption", "se", "Freq")
#%>%rename(c('est_socio'='Socioeconomic','tam_loc'='Locality_Size'))

socio_tam_loc_climate$adoption<-socio_tam_loc_climate$adoption

socio_tam_loc_climate #dataframe/with all data

write.csv(socio_tam_loc_climate,"~/Desktop/MP/mx_electricity_residential/outputs/refri/socio_tam_loc_climate_all.csv", row.names = FALSE)
```