---
title: "2018 AC & Heat"
author: "Amanda Ullman"
date: "10/8/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r Clean Memory/ Auto Load, echo = F}
#Clean Memory
rm(list=ls())

#garbage collector

gc()
#Make packages auto load
if(!require("pacman")) install.packages("pacman")
```

```{r Packages, echo = F}
#load contribute packages with pacman
pacman::p_load(pacman, rio, tidyverse, bit64,forcats,gridExtra,lubridate,cowplot,summarytools,RColorBrewer,survey, scales, ENmisc)
```


```{r Table Set-Up, include = F}

#Source: https://www.inegi.org.mx/programas/enigh/nc/2018/default.html#Microdatos

path.data <- "https://mauricioh2.com/mp2020/inegi/enigh/2018/"

E2018 <-read_csv(paste0(path.data, "2018_enigh_viviendas.csv"))

Gastos2018 <-read_csv(paste0(path.data, "2018_enigh_concentradohogar.csv"))

#Make Special Table
viv <- as_tibble(E2018) 
gasto <-as_tibble(Gastos2018)

viv2<- as.data.frame(E2018)
gasto2 <-as.data.frame(Gastos2018)
ls <- left_join(viv2, gasto2)
  

#Mutate(ubica_geo = as.character(ubica_geo))
ls %>%
  mutate(ubica_geo = as.character(ubica_geo), tipo_viv = as.factor(tipo_viv)) %>%
 filter(!tipo_viv == "&")
```


#Location Coding
```{r, State & Climate Coding, include = F, eval = T} 

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state

ls$state.code <- substr(ls$ubica_geo, 1, 2)

# Create/convert the new variable state into a factor, and assign name of the state as label
ls$state <- factor(ls$state.code, 
                     levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                     labels = c("Aguascalientes", "Baja California", 
                                "Baja California Sur", "Campeche", 
                                "Coahuila", "Colima", "Chiapas", 
                                "Chihuahua", "Mexico City", 
                                "Durango", "Guanajuato", "Guerrero", 
                                "Hidalgo", "Jalisco", "Mexico", 
                                "Michoacan", "Morelos", "Nayarit", 
                                "Nuevo Leon", "Oaxaca", "Puebla", 
                                "Queretaro", "Quintana Roo", 
                                "San Luis Potosi", "Sinaloa", 
                                "Sonora", "Tabasco", "Tamaulipas", 
                                "Tlaxcala", "Veracruz", "Yucatan", 
                                "Zacatecas"))

#Create new variable region into a factor and assign name of region as label
ls$climate <-factor(ls$state.code,
                   levels = c("01", "02", "03", "04", "05", "06", "07", 
                                "08", "09", "10","11", "12", "13", "14", 
                                "15", "16", "17", "18", "19", "20", 
                                "21", "22", "23", "24", "25", "26", 
                                "27", "28", "29", "30", "31", "32"),
                   labels = c("Templada", "Calida.Ext", "Calida.Ext", "Tropical",
                              "Calida.Ext", "Templada", "Tropical", "Calida.Ext",
                              "Templada","Calida.Ext", "Templada", "Tropical",
                              "Templada", "Templada", "Templada", "Templada",
                              "Templada", "Templada", "Calida.Ext", "Tropical", 
                              "Templada", "Templada", "Tropical", "Templada",
                              "Calida.Ext", "Calida.Ext", "Tropical", "Calida.Ext",
                              "Templada", "Templada", "Tropical", "Templada"))

# Extracting the first two characters of the values stored in variable ubica_geo, which correspond to the state

ls$urb.code <- substr(ls$folioviv, 3, 3)

# Create/convert the new variable state into a factor, and assign name of the state as label
ls$urban <- factor(ls$urb.code, 
                     levels = c("0", "1", "2", "3", "4", "5", "6", "7", 
                                "8", "9"),
                     labels = c("Urban", "Urban", "Urban", "Urban", "Urban", "Urban", 
                                "Rural","Urban", "Urban", "Urban"))

#Survey design constructioon to compute statistical error
svd.ls <- svydesign(id =~upm, strata = ~est_socio, data = ls, weight=~factor)

 #Table Test
   ses.table <-svytable(~est_socio+state, design=svd.ls) %>%
     as.data.frame()
  

```
#Air Conditioning  
```{r AC Mexico, include = F}
#1. Determine proportion of households that have air conditioning in Mexico
air.freq <- svytable(~aire_acond, design = svd.ls) %>%
  as.data.frame()
air.freq

air.freq %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),2))) 

air.freq %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),2))) 

air.freq.plot <- ggplot(air.freq, aes(x = aire_acond , y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "Mexico", y = "Frequency", fill = "Air Conditioning") +
          scale_y_continuous(label = comma)+ 
      geom_text(aes(label = c("AC", "No AC")), vjust = -0.5)
```
```{r}
air.freq.plot
```

```{r AC By State, include = F}
#2. Determine proportion of households that have air conditioning by state

air.freq.state <-svytable(~state+aire_acond, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by state
air.freq.state %>%
  group_by(state) %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4))) 
    
      #Make Bar Graph 
      air.freq.state.plot <- ggplot(air.freq.state, aes(x = state, y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "State", y = "Frequency", fill = "Air Conditioning", caption = "Numbers in Millions") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
      + geom_text(aes(label=round(Freq/1000000, 2)),vjust = 2, color="white", 
              position = position_dodge(1),size=3) 
      
      
        #boxplot with weighted factors      
         air.state.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
  
```
```{r}    
      #Print Bar Graph    
    print(air.freq.state.plot) 
       
```

```{r AC By Climate, include = F}
#3. Determine proportion of households that have air conditioning by climate zone
air.freq.climate <-svytable(~climate+aire_acond, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by climate
air.freq.climate %>%
  group_by(climate) %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4))) 

    #Make bar plot
     air.freq.climate.plot <- ggplot(air.freq.climate, aes(x = climate, y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "Climate", y = "Frequency", fill = "Air Conditioning", caption = "Numbers in Millions") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)+
       geom_text(aes(label=round(Freq/1000000, 2)),vjust = 2, color="white", 
              position = position_dodge(1),size=3) 
     
     
        #boxplot with weighted factors      
         air.climate.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
  
```
```{r}    
     #Print bar plot
    print(air.freq.climate.plot) 
       
```     

```{r AC Urban vs Rural, include = F}
#4. Determine proportion of households that have air conditioning: Urban vs Rural
air.freq.urb <-svytable(~urban+aire_acond, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
air.freq.urb %>%
  group_by(urban)%>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
     air.freq.urb.plot <- ggplot(air.freq.urb, aes(x = urban, y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "Area Type", y = "Frequency", fill = "AC", caption = "Numbers in Millions") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma) + 
       geom_text(aes(label=round(Freq/1000000, 2)),vjust = 2, color="white", 
              position = position_dodge(1),size=3) 
     
     
        #boxplot with weighted factors      
         air.urban.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
        
```
```{r}   
     #Print Bar Plot    
    print(air.freq.urb.plot) 
       
```      
  
```{r AC SES, include = F}
#5. Determine proportion of households that have air conditioning: Urban vs Rural
air.freq.ses <-svytable(~est_socio+aire_acond, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
air.freq.ses %>%
  group_by(est_socio)%>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
     air.freq.ses.plot <- ggplot(air.freq.ses, aes(x = est_socio, y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "Socioeconomic Status", y = "Frequency", fill = "AC") +
          scale_y_continuous(label = comma)
     
     
        #boxplot with weighted factors      
         air.ses.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
        
```
```{r}    
     #Print Bar Plot    
    print(air.freq.ses.plot) 
       
```

```{r AC House Type, include = F}
#5. Determine proportion of households that have air conditioning: Urban vs Rural
air.freq.tipo <-svytable(~tipo_viv+aire_acond, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
air.freq.tipo2 <- air.freq.tipo %>%
                   filter(!tipo_viv == "&") %>%
                    group_by(tipo_viv)%>%
                     mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
     air.freq.tipo.plot <- ggplot(air.freq.tipo2, aes(x = tipo_viv, y = Freq, fill = aire_acond)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "viridis", begin = 0.2, end = 0.8) + 
          labs(x = "Housing Type", y = "Frequency", fill = "AC") +
          scale_y_continuous(label = comma)
     
     
        #boxplot with weighted factors      
         air.tipo.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
      

```
```{r}    
     #Print Bar Plot    
    print(air.freq.tipo.plot) 
       
```

#Heating
```{r Heat Mexico, include = F}
#1. Determine proportion of households that have heating in Mexico
cale.freq <- svytable(~calefacc, design = svd.ls) %>%
  as.data.frame()

cale.freq %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),2))) 

cale.freq.plot <- ggplot(cale.freq, aes(x = calefacc , y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Mexico", y = "Frequency", fill = "Heating") +
          scale_y_continuous(label = comma)+ 
      geom_text(aes(label = c("Heat", "No Heat")), vjust = -0.5)
```
```{r}
cale.freq.plot

```

```{r Heat By State, include = F}
#2. Determine proportion of households that have heating by state
cale.freq.state <-svytable(~state+calefacc, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by state
cale.freq.state %>%
  group_by(state) %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4))) 
    
      #Make Bar Graph 
      cale.freq.state.plot <- ggplot(cale.freq.state, aes(x = state, y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "State", y = "Frequency", fill = "Heating") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
      
        #boxplot with weighted factors      
         cale.state.box <- wtd.boxplot(ls$calefacc, weights = ls$factor_viv, plot = TRUE)
         
```
```{r}    
      #Print Bar Graph    
    print(cale.freq.state.plot) 
       
```
```{r Heat By Climate, include = F}
#3. Determine proportion of households that have heating by climate
cale.freq.climate <-svytable(~climate+calefacc, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by climate
cale.freq.climate %>%
  group_by(climate) %>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4))) 

    #Make bar plot
     cale.freq.climate.plot <- ggplot(cale.freq.climate, aes(x = climate, y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Climate", y = "Frequency", fill = "Heating") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
     
     
        #boxplot with weighted factors      
         cale.climate.box <- wtd.boxplot(ls$calefacc, weights = ls$factor_viv, plot = TRUE)
   
         
```
```{r}    
     #Print bar plot
    print(cale.freq.climate.plot) 
       
```

```{r Heat Urban vs. Rural, include = F}
      
#4. Determine proportion of households that have heating: Urban vs Rural
cale.freq.urb <-svytable(~urban+calefacc, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
cale.freq.urb %>%
  group_by(urban)%>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
    cale.freq.urb.plot <- ggplot(cale.freq.urb, aes(x = urban, y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Area Type", y = "Frequency", fill = "Heating") +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
          scale_y_continuous(label = comma)
    
        #boxplot with weighted factors      
         cale.urban.box <- wtd.boxplot(ls$aire_acond, weights = ls$factor_viv, plot = TRUE)
        
```
```{r}   
     #Print Bar Plot    
    print(cale.freq.urb.plot) 
       
```
```{r Heat SES, include = F}

#5. Determine proportion of households that have heater: SES
cale.freq.ses <-svytable(~est_socio+calefacc, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
cale.freq.ses %>%
  group_by(est_socio)%>%
   mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
     cale.freq.ses.plot <- ggplot(cale.freq.ses, aes(x = est_socio, y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Socioeconomic Status", y = "Frequency", fill = "Heating") +
          scale_y_continuous(label = comma)
     
      #boxplot with weighted factors      
         cale.ses.box <- wtd.boxplot(ls$calefacc, weights = ls$factor_viv, plot = TRUE)
        
```
```{r}    
     #Print Bar Plot    
    print(cale.freq.ses.plot) 
       
       
```

```{r Heat House Type, include = F}
#6. Determine proportion of households that have heater: House Type
cale.freq.tipo <-svytable(~tipo_viv+calefacc, design = svd.ls) %>%
      as.data.frame()
      
#Find relative proportions by area type
cale.freq.tipo2 <- cale.freq.tipo %>%
                   filter(!tipo_viv == "&") %>%
                    group_by(tipo_viv)%>%
                     mutate(`Percentage (%)`  = (100*round(Freq / sum(Freq),4)))

    #Make Bar Plot
     cale.freq.tipo.plot <- ggplot(cale.freq.tipo2, aes(x = tipo_viv, y = Freq, fill = calefacc)) +
          geom_bar(stat="identity") +
          scale_fill_viridis_d(option = "plasma", begin = 0.2, end = 0.6) + 
          labs(x = "Housing Type", y = "Frequency", fill = "Heating") +
          scale_y_continuous(label = comma)
     
     
        #boxplot with weighted factors      
         cale.tipo.box <- wtd.boxplot(ls$calefacc, weights = ls$factor_viv, plot = TRUE)

```
```{r}    
     #Print Bar Plot    
    print(cale.freq.tipo.plot) 
       
```